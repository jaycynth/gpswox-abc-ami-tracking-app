package com.abcami.gpswox_tracking_app.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.abcami.gpswox_tracking_app.MainActivity;
import com.abcami.gpswox_tracking_app.R;
import com.abcami.gpswox_tracking_app.data.model.Entry;
import com.abcami.gpswox_tracking_app.ui.maps.MapsFragment;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import static com.abcami.gpswox_tracking_app.utils.Constants.JOB_ID;
import static com.abcami.gpswox_tracking_app.utils.Constants.NOTIFICATION_CHANNEL_ID;

public class GeofenceTransitionsJobIntentService extends JobIntentService {

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceErrorMessages.getErrorString(this,
                    geofencingEvent.getErrorCode());
            Log.e("LOG_TAG", errorMessage);
            return;
        }
        handleEvent(geofencingEvent);
    }

    private void handleEvent(GeofencingEvent event) {
        if (event.getGeofenceTransition() == Geofence.GEOFENCE_TRANSITION_ENTER) {
            Entry reminder = getFirstReminder(event.getTriggeringGeofences());
            String message = reminder.getMessage();
            LatLng latLng = reminder.getLatLng();
            if (message != null && latLng != null) {
                sendNotification(this, message, latLng);
            }
        }
    }

    private void sendNotification(Context context, String message, LatLng latLng) {

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
                && notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID) == null) {
            String name = context.getString(R.string.app_name);
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    name,
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);
        }

        Intent intent = MapsFragment.start(context.getApplicationContext(), latLng);


        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context)
                .addParentStack(MainActivity.class)
                .addNextIntent(intent);

        PendingIntent notificationPendingIntent = stackBuilder
                .getPendingIntent(getUniqueId(), PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(message)
                .setContentIntent(notificationPendingIntent)
                .setAutoCancel(true)
                .build();

        if (notificationManager != null) {
            notificationManager.notify(getUniqueId(), notification);
        }
    }

    private Entry getFirstReminder(List<Geofence> triggeringGeofences) {
        Geofence firstGeofence = triggeringGeofences.get(0);
        return (((TrackerApp) getApplication()).getGeofenceEntryRepo().get(firstGeofence.getRequestId()));
    }

    private Integer getUniqueId() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Math.toIntExact(System.currentTimeMillis() % 10000);
        } else {
            return (int) System.currentTimeMillis() % 10000;
        }
    }


    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(
                context,
                GeofenceTransitionsJobIntentService.class, JOB_ID,
                intent);
    }
}

