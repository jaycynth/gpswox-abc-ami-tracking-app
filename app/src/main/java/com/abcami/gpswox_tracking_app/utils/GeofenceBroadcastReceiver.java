package com.abcami.gpswox_tracking_app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static com.abcami.gpswox_tracking_app.utils.GeofenceTransitionsJobIntentService.*;

public class GeofenceBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        enqueueWork(context, intent);


    }
}
