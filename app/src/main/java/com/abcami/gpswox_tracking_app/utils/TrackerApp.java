package com.abcami.gpswox_tracking_app.utils;

import android.app.Application;
import android.content.Context;

import com.abcami.gpswox_tracking_app.data.repositories.GeofenceEntryRepo;


public class TrackerApp extends Application {

    private GeofenceEntryRepo geofenceEntryRepo;
    public static Context context;


    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
        //MultiDex.install(this);
        geofenceEntryRepo = new GeofenceEntryRepo(this);
    }

    public  GeofenceEntryRepo getGeofenceEntryRepo() {
        return geofenceEntryRepo;
    }


}
