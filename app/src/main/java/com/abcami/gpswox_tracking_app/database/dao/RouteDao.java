package com.abcami.gpswox_tracking_app.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.abcami.gpswox_tracking_app.data.model.AddedRoute;

import java.util.List;

@Dao
public interface RouteDao {

    @Insert
    void insert(AddedRoute addedRoute);

    @Delete
    void delete(AddedRoute addedRoute);

    @Query("SELECT * FROM AddedRoute")
    List<AddedRoute> getAllRoutes();
}
