package com.abcami.gpswox_tracking_app.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.abcami.gpswox_tracking_app.data.model.AddedRoute;
import com.abcami.gpswox_tracking_app.database.dao.RouteDao;
import com.abcami.gpswox_tracking_app.utils.Constants;

@Database(entities = {AddedRoute.class}, version = 2,exportSchema = false)
public abstract class TrackingDatabase extends RoomDatabase {

    private static volatile TrackingDatabase INSTANCE;
    private static final Callback callback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

        }
    };

    public static TrackingDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), TrackingDatabase.class, Constants.DB_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }



    //all dao
    public abstract RouteDao routeDao();


}
