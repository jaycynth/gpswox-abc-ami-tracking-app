package com.abcami.gpswox_tracking_app.ui;



import androidx.fragment.app.Fragment;

import com.abcami.gpswox_tracking_app.data.repositories.GeofenceEntryRepo;
import com.abcami.gpswox_tracking_app.utils.TrackerApp;



public abstract class BaseFragment extends Fragment {

    public GeofenceEntryRepo getRepository() {
        return ((TrackerApp) requireActivity().getApplication()).getGeofenceEntryRepo();
    }

}