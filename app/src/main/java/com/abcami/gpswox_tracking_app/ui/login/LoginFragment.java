package com.abcami.gpswox_tracking_app.ui.login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.abcami.gpswox_tracking_app.R;
import com.abcami.gpswox_tracking_app.data.model.UserLogin;
import com.abcami.gpswox_tracking_app.utils.SharedPrefManager;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginFragment extends Fragment {

    @BindView(R.id.username)
    TextInputEditText username;

    @BindView(R.id.password)
    TextInputEditText password;

    @BindView(R.id.login)
    Button login_btn;

    @BindView(R.id.loading)
    ProgressBar loading_progress_bar;

    private NavController navController;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        navController = NavHostFragment.findNavController(this);


        LoginViewModel loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);


        loginViewModel.userLoginResponse().observe(getViewLifecycleOwner(), userLoginState -> {
            if (userLoginState.getUserLogin() != null) {
                userLogin(userLoginState.getUserLogin());
            }

            if (userLoginState.getMessage() != null) {
                messageError(userLoginState.getMessage());
            }

            if (userLoginState.getThrowable() != null) {
                ThrowableError(userLoginState.getThrowable());
            }
        });


        login_btn.setOnClickListener(v -> {

            String mEmail = Objects.requireNonNull(username.getText()).toString().trim();
            String mPassword = Objects.requireNonNull(password.getText()).toString().trim();

            if (TextUtils.isEmpty(mEmail)) {
                Toast.makeText(getActivity(), "Enter email", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mPassword)) {
                Toast.makeText(getActivity(), "Enter password", Toast.LENGTH_SHORT).show();
            } else {
                loading_progress_bar.setVisibility(View.VISIBLE);
                loginViewModel.userLogin(mEmail, mPassword);
            }
        });

        return view;

    }

    private void userLogin(UserLogin userLogin) {
        loading_progress_bar.setVisibility(View.GONE);
        int status = userLogin.getStatus();
        if (status == 1) {
            //store api hash key
            SharedPrefManager.getInstance(getActivity()).saveApiHashKey(userLogin.getUserApiHash());

            //send user to map fragment
            navController.navigate(R.id.mapsFragment);
        }
    }

    private void messageError(String message) {
        loading_progress_bar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }


    private void ThrowableError(Throwable throwable) {
        loading_progress_bar.setVisibility(View.GONE);
        if (throwable instanceof IOException) {
            Toast.makeText(getActivity(), "network issue", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "conversion issue", Toast.LENGTH_SHORT).show();
        }
    }


}