package com.abcami.gpswox_tracking_app.ui.maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.abcami.gpswox_tracking_app.MainActivity;
import com.abcami.gpswox_tracking_app.PlacesAutoCompleteAdapter;
import com.abcami.gpswox_tracking_app.R;
import com.abcami.gpswox_tracking_app.data.model.AddRoute;
import com.abcami.gpswox_tracking_app.data.model.CalculateDirections;
import com.abcami.gpswox_tracking_app.data.model.Coordinate;
import com.abcami.gpswox_tracking_app.data.model.DestroyRoute;
import com.abcami.gpswox_tracking_app.data.model.GeocodeAddress;
import com.abcami.gpswox_tracking_app.data.model.GetRoutes;
import com.abcami.gpswox_tracking_app.data.model.GetDevices;
import com.abcami.gpswox_tracking_app.data.model.Item;
import com.abcami.gpswox_tracking_app.data.model.PolylineData;
import com.abcami.gpswox_tracking_app.data.model.Route;
import com.abcami.gpswox_tracking_app.ui.BaseFragment;
import com.abcami.gpswox_tracking_app.utils.ClusterMarker;
import com.abcami.gpswox_tracking_app.utils.MyClusterManagerRenderer;
import com.abcami.gpswox_tracking_app.utils.SharedPrefManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.google.maps.GeoApiContext;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.internal.PolylineEncoding;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.abcami.gpswox_tracking_app.utils.Utils.resetMap;
import static com.abcami.gpswox_tracking_app.utils.Utils.zoomArea;
import static com.abcami.gpswox_tracking_app.utils.Utils.zoomRoute;

public class MapsFragment extends BaseFragment implements OnMapReadyCallback,
        ClusterManager.OnClusterItemInfoWindowClickListener<ClusterMarker>, GoogleMap.OnPolylineClickListener {

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;


    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.map_container)
    CoordinatorLayout map_fragment;


    private MapsViewModel mapsViewModel;

    private List<Item> allItems = new ArrayList<>();

    private ClusterManager<ClusterMarker> mClusterManager;
    private MyClusterManagerRenderer mClusterManagerRenderer;
    private ArrayList<ClusterMarker> mClusterMarkers = new ArrayList<>();

    private List<LatLng> latLngList = new ArrayList<>();

    private String gpswoxToken;

    //calculating directions
    private GeoApiContext mGeoApiContext = null;
    private LatLng currentPosition;

    //polylines
    private ArrayList<PolylineData> mPolylineData = new ArrayList<>();
    private ArrayList<Marker> mTripMarkers = new ArrayList<>();
    private ClusterMarker mSelectedMarker = null;

    private List<Route> routes = new ArrayList<>();

    PlacesClient placesClient;

    PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private String currentAddress, add;

    private String routeTitle;

    private NavController navController;

    private List<String> clusterNames = new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);
        ButterKnife.bind(this, view);

        navController = NavHostFragment.findNavController(this);

        //TODO 1.delete token on logout
        gpswoxToken = SharedPrefManager.getInstance(getActivity()).getApiHashKey();

        if (gpswoxToken == null) {
            navController.navigate(R.id.loginFragment);
        }

        mapsViewModel = new ViewModelProvider(this).get(MapsViewModel.class);


        boolean permissionGranted = ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (!permissionGranted) {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
        }


        if (!Places.isInitialized()) {
            Places.initialize(requireActivity().getApplicationContext(), getString(R.string.google_maps_key));
        }

        placesClient = Places.createClient(requireActivity());


        mapsViewModel.getDevicesResponse().observe(getViewLifecycleOwner(), getDevicesState -> {
            if (getDevicesState.getAllDevices() != null) {
                getDevices(getDevicesState.getAllDevices());
            }

            if (getDevicesState.getMessage() != null) {
                ErrorMessage(getDevicesState.getMessage());
            }

            if (getDevicesState.getThrowable() != null) {
                ErrorThrowable(getDevicesState.getThrowable());
            }
        });

        mapsViewModel.addRouteResponse().observe(getViewLifecycleOwner(), addRouteState -> {
            if (addRouteState.getAddRoute() != null) {
                addRoute(addRouteState.getAddRoute());
            }

            if (addRouteState.getMessage() != null) {
                ErrorMessage(addRouteState.getMessage());
            }

            if (addRouteState.getThrowable() != null) {
                ErrorThrowable(addRouteState.getThrowable());
            }
        });

        mapsViewModel.getRoutesResponse().observe(getViewLifecycleOwner(), getRouteState -> {
            if (getRouteState.getRouteState() != null) {
                getRoutes(getRouteState.getRouteState());
            }

            if (getRouteState.getMessage() != null) {
                ErrorMessage(getRouteState.getMessage());
            }

            if (getRouteState.getThrowable() != null) {
                ErrorThrowable(getRouteState.getThrowable());
            }
        });

        mapsViewModel.destroyRouteResponse().observe(getViewLifecycleOwner(), destroyRouteState -> {
            if (destroyRouteState.getDestroyRoute() != null) {
                destroyRoutes(destroyRouteState.getDestroyRoute());
            }

            if (destroyRouteState.getMessage() != null) {
                ErrorMessage(destroyRouteState.getMessage());
            }

            if (destroyRouteState.getThrowable() != null) {
                ErrorThrowable(destroyRouteState.getThrowable());
            }
        });

        mapsViewModel.getGeocodeResponse().observe(getViewLifecycleOwner(), geocodeAddressState -> {

            if (geocodeAddressState.getGeocodeAddress() != null) {
                geocodeAddress(geocodeAddressState.getGeocodeAddress());
            }

            if (geocodeAddressState.getMessage() != null) {
                ErrorMessage(geocodeAddressState.getMessage());
            }

            if (geocodeAddressState.getThrowable() != null) {
                ErrorThrowable(geocodeAddressState.getThrowable());
            }
        });


        mapsViewModel.calculateDirectionsResponse().observe(getViewLifecycleOwner(), calculateDirectionState -> {
            if (calculateDirectionState.getCalculateDirections() != null) {
                calcDirections(calculateDirectionState.getCalculateDirections());
            }

            if (calculateDirectionState.getMessage() != null) {
                ErrorMessage(calculateDirectionState.getMessage());
            }

            if (calculateDirectionState.getThrowable() != null) {
                ErrorThrowable(calculateDirectionState.getThrowable());
            }
        });

        if (mapFragment == null) {
            mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map_fragment);
            assert mapFragment != null;
            mapFragment.getMapAsync(this);
        }


        if (mGeoApiContext == null) {
            mGeoApiContext = new GeoApiContext.Builder().apiKey(getString(R.string.google_maps_key)).build();
        }

        mapsViewModel.getRoutes(gpswoxToken);

        return view;
    }

    private void geocodeAddress(GeocodeAddress geocodeAddress) {

        currentAddress = geocodeAddress.getLocationData().getResults().get(0).getFormattedAddress();
    }

    private void calcDirections(CalculateDirections calculateDirections) {
        new Handler(Looper.getMainLooper()).post(() -> {
            //CLICKING AND HIGHLIGHTING
            if (mPolylineData.size() > 0) {
                for (PolylineData polylineData : mPolylineData) {
                    polylineData.getPolyline().remove();
                }
                mPolylineData.clear();
                mPolylineData = new ArrayList<>();
            }

            double duration = 99999999;
            for (Route route : calculateDirections.getLocationData().getRoutes()) {
                List<com.google.maps.model.LatLng> decodedPath = PolylineEncoding.decode(route.getOverviewPolyline().getPoints());

                List<LatLng> newDecodedPath = new ArrayList<>();

                // This loops through all the LatLng coordinates of ONE polyline.
                for (com.google.maps.model.LatLng latLng : decodedPath) {
                    newDecodedPath.add(new LatLng(
                            latLng.lat,
                            latLng.lng
                    ));
                }
                com.google.android.gms.maps.model.Polyline polyline = mMap.addPolyline(new PolylineOptions().addAll(newDecodedPath));
                polyline.setColor(ContextCompat.getColor(requireActivity(), R.color.red));
                polyline.setClickable(true);
                mPolylineData.add(new PolylineData(polyline, route.getLegs().get(0)));

                String dur = route.getLegs().get(0).getDuration().getText();

                double tempdur = Double.parseDouble(dur.substring(0, 1));

                if (tempdur < duration) {
                    duration = tempdur;

                    onPolylineClick(polyline);
                    zoomRoute(mMap, polyline.getPoints());
                }
            }
        });
    }


    public static Intent start(Context context, LatLng latLng) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("EXTRA_LAT_LNG", latLng);
        return intent;
    }


    /*....methods for endpoint responses....*/
    private void destroyRoutes(DestroyRoute destroyRoute) {
        if (destroyRoute.getStatus() == 1) {
            Toast.makeText(getActivity(), "Route Destroyed Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    private void getRoutes(GetRoutes routeState) {
        if (routeState.getStatus() == 1) {
            routes = routeState.getRoutes();
        }
    }

    private void addRoute(AddRoute addRoute) {
        if (addRoute.getStatus() == 1) {
            Toast.makeText(getActivity(), "Route added successfully", Toast.LENGTH_SHORT).show();
        }
    }


    private void getDevices(List<GetDevices> allDevices) {
        progressBar.setVisibility(View.GONE);
        map_fragment.setAlpha(1);
        for (GetDevices getDevices : allDevices) {
            allItems = getDevices.getItems();
            addMapMarker(allItems);
        }
    }

    private void ErrorMessage(String message) {
        progressBar.setVisibility(View.GONE);
        map_fragment.setAlpha(1);

        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private void ErrorThrowable(Throwable throwable) {
        progressBar.setVisibility(View.GONE);
        map_fragment.setAlpha(1);

        if (throwable instanceof IOException) {
            Toast.makeText(getActivity(), "network issue", Toast.LENGTH_SHORT).show();
        } else {
            Log.d("CONVERSION ERROR", Objects.requireNonNull(throwable.getMessage()));
            Toast.makeText(getActivity(), "conversion error", Toast.LENGTH_SHORT).show();
        }
    }

    /*Map code*/
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

        if (allItems != null && !allItems.isEmpty()) {
            addMapMarker(allItems);
        } else {
            map_fragment.setAlpha(0.3f);
            progressBar.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "Loading....", Toast.LENGTH_SHORT).show();
            mapsViewModel.getDevices(gpswoxToken);
        }

    }


    private void addMapMarker(List<Item> items) {
        //loop all users and create a cluster item for them
        if (mMap != null) {
            resetMap(mMap, mClusterManager, mClusterMarkers);
            if (mClusterManager == null) {
                //initialize cluster manager
                mClusterManager = new ClusterManager<>(requireActivity().getApplicationContext(), mMap);
            }

            mMap.setOnInfoWindowClickListener(mClusterManager);
            mMap.setOnMarkerClickListener(mClusterManager);

            mMap.setOnInfoWindowClickListener(mClusterManager);

            mClusterManager.setOnClusterItemInfoWindowClickListener(this);


            if (mClusterManagerRenderer == null) {
                mClusterManagerRenderer = new MyClusterManagerRenderer(
                        getActivity(),
                        mMap,
                        mClusterManager
                );
                mClusterManager.setRenderer(mClusterManagerRenderer);
            }

            for (Item item : items) {

                Log.d("MAP MARKERS", "addMapMarkers: location: " + item.getLat() + item.getLng());
                try {
                    String snippet = "Choose Action";
                    int avatar;

                    if (item.getOnline().equals("online")) {
                        avatar = R.drawable.ic_green;
                    } else if (item.getOnline().equals("ack")) {
                        avatar = R.drawable.ic_orange;
                    } else {
                        avatar = R.drawable.ic_rec;
                    }
                    ClusterMarker newClusterMarker = new ClusterMarker(
                            new LatLng(item.getLat(), item.getLng()),
                            item.getName(),
                            snippet,
                            avatar,
                            item.getAddress());

                    mClusterManager.addItem(newClusterMarker);
                    mClusterMarkers.add(newClusterMarker);

                } catch (NullPointerException e) {
                    Log.e("MAP MARKERS", "addMapMarkers: NullPointerException: " + e.getMessage());
                }

                latLngList.add(new LatLng(item.getLat(), item.getLng()));

            }
            mClusterManager.cluster();
            zoomArea(mMap, latLngList);
        }
    }


    @Override
    public void onClusterItemInfoWindowClick(ClusterMarker item) {
        mSelectedMarker = item;

        routeTitle = item.getTitle();

        currentPosition = item.getPosition();

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPosition, 15f));

        mapsViewModel.geocodeAddress(item.getPosition().latitude, item.getPosition().longitude);

        chooseActionDialog();
    }

    private void chooseActionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setTitle("Choose Action");
        builder.setMessage("Track or Add Route..?");
        builder.setPositiveButton(R.string.track, ((dialog, which) -> {
            MapsFragmentDirections.ActionMapsFragmentToRealTimeTrackMapFragment action =
                    MapsFragmentDirections.actionMapsFragmentToRealTimeTrackMapFragment().setRouteTitle(routeTitle);
            navController.navigate(action);
            dialog.dismiss();

        }));
        builder.setNegativeButton(R.string.add_route, ((dialog, which) -> {
            destAlertDialog();
            dialog.dismiss();
        }));
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void destAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setTitle("Add  Destination");
        final View customLayout = getLayoutInflater().inflate(R.layout.cluster_item_click_layout, null);
        builder.setView(customLayout);
        AutoCompleteTextView addressValue = customLayout.findViewById(R.id.search_for_dest);
        RecyclerView recyclerView = customLayout.findViewById(R.id.places_recycler_view);

        TextWatcher filterTextWatcher = new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    mAutoCompleteAdapter.getFilter().filter(s.toString());
                    if (recyclerView.getVisibility() == View.GONE) {
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (recyclerView.getVisibility() == View.VISIBLE) {
                        recyclerView.setVisibility(View.GONE);
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        };


        addressValue.addTextChangedListener(filterTextWatcher);
        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAutoCompleteAdapter.setClickListener(place -> addressValue.setText(place.getAddress()));
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();


        builder.setPositiveButton(R.string.ok, (dialog, which) -> {
            add = Objects.requireNonNull(addressValue.getText()).toString().trim();

            if (TextUtils.isEmpty(add)) {
                Toast.makeText(getActivity(), "Enter Dest Address", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Loading...", Toast.LENGTH_LONG).show();
                resetSelectedMarker();
                mapsViewModel.calculateDirections(currentAddress, add);
            }
        });
        builder.setNegativeButton(R.string.cancel, ((dialog, which) -> dialog.dismiss()));
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void onPolylineClick(com.google.android.gms.maps.model.Polyline polyline) {
        int index = 0;
        for (PolylineData polylineData : mPolylineData) {
            index++;
            Log.d("POLYLINE CLICK", "onPolylineClick: toString: " + polylineData.toString());
            if (polyline.getId().equals(polylineData.getPolyline().getId())) {
                polylineData.getPolyline().setColor(ContextCompat.getColor(requireActivity(), R.color.blue));
                polylineData.getPolyline().setZIndex(1);

                LatLng endLocation = new LatLng(
                        polylineData.getLeg().getEndLocation().getLat(),
                        polylineData.getLeg().getEndLocation().getLng()
                );

                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(endLocation)
                        .title("Trip:# " + index)
                        .snippet("Duration: " + polylineData.getLeg().getDuration().getText()));

                marker.showInfoWindow();

                mTripMarkers.add(marker);

                List<LatLng> points = polylineData.getPolyline().getPoints();
                ArrayList<Coordinate> decoded = new ArrayList<>();

                for (LatLng l : points) {
                    decoded.add(new Coordinate(l.latitude, l.longitude));
                }
                Gson gson = new Gson();

                mapsViewModel.addRoute(gpswoxToken, routeTitle, getString(R.string.route_color),
                        gson.toJson(decoded));

            } else {
                polylineData.getPolyline().setColor(ContextCompat.getColor(requireActivity(), R.color.red));
                polylineData.getPolyline().setZIndex(0);
            }
        }

    }


    private void removeTripMarkers() {
        for (Marker marker : mTripMarkers) {
            marker.remove();
        }
    }

    private void resetSelectedMarker() {
        if (mSelectedMarker != null) {
            mSelectedMarker = null;
            //TODO destroy routes
            for (Route route : routes) {
                if (route.getName().equals(routeTitle)) {
                    mapsViewModel.destroyRoute(gpswoxToken, route.getId());
                }
            }
            removeTripMarkers();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.maps_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refresh) {
            progressBar.setVisibility(View.VISIBLE);
            map_fragment.setAlpha(0.3f);
            mapsViewModel.getDevices(gpswoxToken);
        } else if (id == R.id.add_geofence) {

            navController.navigate(R.id.action_mapsFragment_to_geofenceMapFragment);
        } else if (id == R.id.filter_devices) {
            for (ClusterMarker clusterMarker : mClusterMarkers) {
                clusterNames.add(clusterMarker.getTitle());
            }
            showDevicesDropDown(clusterNames);

        }
        return super.onOptionsItemSelected(item);
    }

    private void showDevicesDropDown(List<String> clusterNames) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setTitle("Choose Device:");
        final View customLayout = getLayoutInflater().inflate(R.layout.device_filter, null);
        builder.setView(customLayout);
        AutoCompleteTextView deviceFilter = customLayout.findViewById(R.id.device_filter);
        ImageView deviceFilterDropDown = customLayout.findViewById(R.id.device_filter_drop_down);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(requireActivity(), android.R.layout.simple_dropdown_item_1line, clusterNames);
        deviceFilter.setAdapter(adapter1);
        deviceFilter.setOnClickListener(v -> deviceFilter.showDropDown());
        deviceFilterDropDown.setOnClickListener(v -> deviceFilter.showDropDown());

        deviceFilter.setOnItemClickListener((parent, view, position, id) -> {
            String name = (String) parent.getItemAtPosition(position);

            deviceFilter.setText(name);

        });
        builder.setPositiveButton(R.string.ok, ((dialog, which) -> {
            String name = deviceFilter.getText().toString().trim();
            if (TextUtils.isEmpty(name)) {
                Toast.makeText(getActivity(), "Filter field is empty", Toast.LENGTH_SHORT).show();
            } else {
                if (mClusterMarkers != null && !mClusterMarkers.isEmpty()) {
                    for (ClusterMarker clusterMarker : mClusterMarkers) {
                        assert clusterMarker.getTitle() != null;
                        if (clusterMarker.getTitle().equals(name)) {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(clusterMarker.getPosition(), 15f));

                        }
                    }
                }
            }
        }));
        builder.setNegativeButton(R.string.cancel, ((dialog, which) -> dialog.dismiss()));
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}