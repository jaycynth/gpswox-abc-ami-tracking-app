package com.abcami.gpswox_tracking_app.ui.maps;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abcami.gpswox_tracking_app.R;

public class RealtimeMap extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_realtime_map, container, false);
       return view;
    }
}