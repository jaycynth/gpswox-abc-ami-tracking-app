package com.abcami.gpswox_tracking_app.ui.maps;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.abcami.gpswox_tracking_app.R;
import com.abcami.gpswox_tracking_app.data.model.AddGeofence;
import com.abcami.gpswox_tracking_app.data.model.AddGeofenceErrors;
import com.abcami.gpswox_tracking_app.data.model.DestroyGeofence;
import com.abcami.gpswox_tracking_app.data.model.Entry;
import com.abcami.gpswox_tracking_app.data.model.Errors;
import com.abcami.gpswox_tracking_app.data.model.Geofence;
import com.abcami.gpswox_tracking_app.data.model.GetGeofences;
import com.abcami.gpswox_tracking_app.data.model.Items;
import com.abcami.gpswox_tracking_app.ui.BaseFragment;
import com.abcami.gpswox_tracking_app.utils.RepoResult;
import com.abcami.gpswox_tracking_app.utils.SharedPrefManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.abcami.gpswox_tracking_app.utils.Utils.hideKeyboard;
import static com.abcami.gpswox_tracking_app.utils.Utils.showReminderInMap;


public class GeofenceMapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;

    @BindView(R.id.marker)
    ImageView marker;

    @BindView(R.id.container_form)
    LinearLayout container_form;

    @BindView(R.id.instructionTitle)
    TextView instruction_title;

    @BindView(R.id.instructionSubtitle)
    TextView instruction_subtitle;

    @BindView(R.id.radiusBar)
    SeekBar radius_bar;

    @BindView(R.id.radiusDescription)
    TextView radius_description;

    @BindView(R.id.message)
    EditText message;

    @BindView(R.id.next)
    Button next;

    MapsViewModel mapsViewModel;
    String token;


    private Entry entry;

    List<Geofence> gpswoxGeofences = new ArrayList<>();

    @BindView(R.id.add_another_geofence)
    FloatingActionButton add_another_geofence;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_geofence_map, container, false);
        ButterKnife.bind(this, view);

        mapsViewModel = new ViewModelProvider(this).get(MapsViewModel.class);

        token = SharedPrefManager.getInstance(getActivity()).getApiHashKey();

        entry = new Entry(null, null, null);

        if (mapFragment == null) {
            mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.geofence_map);
            mapFragment.getMapAsync(this);
        }


        mapsViewModel.addGeofenceResponse().observe(getViewLifecycleOwner(), addGeofenceState -> {
            if (addGeofenceState.getAddGeofence() != null) {
                addGeofence(addGeofenceState.getAddGeofence());
            }
            if (addGeofenceState.getAddGeofenceErrors() != null) {
                addGeofenceBadRequest(addGeofenceState.getAddGeofenceErrors());
            }
            if (addGeofenceState.getMessage() != null) {
                ErrorMessage(addGeofenceState.getMessage());
            }
            if (addGeofenceState.getThrowable() != null) {
                ErrorThrowable(addGeofenceState.getThrowable());
            }
        });

        mapsViewModel.getGeofencesResponse().observe(getViewLifecycleOwner(), getGeofencesState -> {
            if (getGeofencesState.getGeofences() != null) {
                getGoefences(getGeofencesState.getGeofences());
            }

            if (getGeofencesState.getMessage() != null) {
                ErrorMessage(getGeofencesState.getMessage());
            }

            if (getGeofencesState.getThrowable() != null) {
                ErrorThrowable(getGeofencesState.getThrowable());
            }
        });

        mapsViewModel.destroyGeofenceResponse().observe(getViewLifecycleOwner(), destroyGeofenceState -> {
            if (destroyGeofenceState.getDestroyGeofence() != null) {
                destroyGeofence(destroyGeofenceState.getDestroyGeofence());
            }
            if (destroyGeofenceState.getMessage() != null) {
                ErrorMessage(destroyGeofenceState.getMessage());
            }
            if (destroyGeofenceState.getThrowable() != null) {
                ErrorThrowable(destroyGeofenceState.getThrowable());
            }
        });

        if (mMap != null) {
            Toast.makeText(getActivity(), "trueeee", Toast.LENGTH_SHORT).show();
            showReminders();
        }

        add_another_geofence.setOnClickListener(v -> showConfigureLocationStep());

        return view;
    }


    private void destroyGeofence(DestroyGeofence destroyGeofence) {
        if (destroyGeofence.getStatus() == 1) {
            Toast.makeText(getActivity(), "Destroy successful", Toast.LENGTH_SHORT).show();
        }

    }

    private void getGoefences(GetGeofences geofences) {
        if (geofences.getStatus() == 1) {
            Items items = geofences.getItems();
            gpswoxGeofences = items.getGeofences();
        }
    }


    private void addGeofenceBadRequest(AddGeofenceErrors addGeofenceErrors) {
        if (addGeofenceErrors.getStatus() == 0) {
            Errors errors = addGeofenceErrors.getErrors();
            List<String> centreError = errors.getCenter();
            if (centreError != null) {
                for (String e : centreError) {
                    Toast.makeText(getActivity(), e, Toast.LENGTH_SHORT).show();
                }
            }
            List<String> polygonColorErrors = errors.getPolygonColor();
            if (polygonColorErrors != null) {
                for (String e : polygonColorErrors) {
                    Toast.makeText(getActivity(), e, Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    private void addGeofence(AddGeofence addGeofence) {
        if (addGeofence.getStatus() == 1) {
            Toast.makeText(getActivity(), "Added Geofence Successfully", Toast.LENGTH_LONG).show();
            mapsViewModel.getGeofences(token);

        }
    }

    private void ErrorMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private void ErrorThrowable(Throwable throwable) {
        if (throwable instanceof IOException) {
            Toast.makeText(getActivity(), "network issue", Toast.LENGTH_SHORT).show();
        } else {

            Log.d("CONVERSION ERROR", Objects.requireNonNull(throwable.getMessage()));
            Toast.makeText(getActivity(), "conversion error", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);

        showConfigureLocationStep();

    }


    private void showConfigureMessageStep() {

        marker.setVisibility(View.GONE);
        instruction_title.setVisibility(View.VISIBLE);
        instruction_subtitle.setVisibility(View.GONE);
        radius_bar.setVisibility(View.GONE);
        radius_description.setVisibility(View.GONE);
        message.setVisibility(View.VISIBLE);
        instruction_title.setText(getString(R.string.instruction_message_description));


        next.setOnClickListener(v -> {

            hideKeyboard(requireActivity(), message);

            entry.setMessage(message.getText().toString());

            if (TextUtils.isEmpty(entry.getMessage())) {
                message.setError(getString(R.string.error_required));
            } else {
                mapsViewModel.addGeofence(token, entry.getTYPE(), entry.getMessage(), entry.getPOLYGON_COLOR(),
                        (int) entry.getRadius(), entry.getLatLng());
                addReminder(entry);
            }
        });

        message.requestFocus();
        showReminderUpdate();
    }

    private void showConfigureRadiusStep() {
        marker.setVisibility(View.GONE);
        instruction_title.setVisibility(View.VISIBLE);
        instruction_subtitle.setVisibility(View.GONE);
        radius_bar.setVisibility(View.VISIBLE);
        radius_description.setVisibility(View.VISIBLE);
        message.setVisibility(View.GONE);

        instruction_title.setText(getString(R.string.instruction_radius_description));

        next.setOnClickListener(v -> showConfigureMessageStep());

        seekBarControl();

        updateRadiusWithProgress(radius_bar.getProgress());

        mMap.animateCamera(CameraUpdateFactory.zoomTo(15f));

        showReminderUpdate();
    }


    private void seekBarControl() {
        radius_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateRadiusWithProgress(progress);
                showReminderUpdate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void updateRadiusWithProgress(int progress) {
        Double radius = getRadius(progress);
        entry.setRadius(radius);
        radius_description.setText(getString(R.string.radius_description, radius.toString()));
    }

    private Double getRadius(int progress) {
        return 100 + (2 * (double) progress + 1) * 100;
    }

    private void addReminder(Entry entry) {
        //add reminder in both
        RepoResult repoResult = (error, successMessage) -> {
            if (error != null) {
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
            } else {
                requireActivity().setResult(Activity.RESULT_OK);
                Toast.makeText(getActivity(), successMessage, Toast.LENGTH_SHORT).show();
            }
            showConfigureLocationStep();
        };
        getRepository().add(entry, repoResult);

    }

    private void showReminderUpdate() {
        mMap.clear();
        showReminderInMap(getActivity(), mMap, entry);
    }


    private void showConfigureLocationStep() {
        marker.setVisibility(View.VISIBLE);
        instruction_title.setVisibility(View.VISIBLE);
        instruction_subtitle.setVisibility(View.VISIBLE);
        radius_bar.setVisibility(View.GONE);
        radius_description.setVisibility(View.GONE);
        message.setVisibility(View.GONE);
        instruction_title.setText(getString(R.string.instruction_where_description));

        next.setOnClickListener(v -> {
            entry.setLatLng(mMap.getCameraPosition().target);
            showConfigureRadiusStep();
        });

        showReminderUpdate();
    }


    private void showReminderRemoveAlert(Entry entry) {

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setMessage(getString(R.string.reminder_removal_alert));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", (dialog, which) -> {
            removeReminder(entry);
            alertDialog.dismiss();
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                "CANCEL", (dialog, which) -> alertDialog.dismiss());
        alertDialog.show();

    }

    private void removeReminder(Entry entry) {

        int gpswoxGeofenceId = 0;

        for (Geofence g : gpswoxGeofences) {

            if (g.getName().trim().equals(entry.getMessage().trim())) {
                gpswoxGeofenceId = g.getId();
            }
        }

        mapsViewModel.destroyGeofence(token, gpswoxGeofenceId);

        RepoResult repoResult = (error, successMessage) -> {
            if (error != null) {
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
            } else {
                showReminders();
                Toast.makeText(getActivity(), successMessage, Toast.LENGTH_SHORT).show();
            }
        };
        getRepository().remove(entry, repoResult);


    }


    private void showReminders() {
        mMap.clear();
        for (Entry e : getRepository().getAll()) {
            showReminderInMap(getActivity(), mMap, e);
        }

    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        Entry entry = getRepository().get(Objects.requireNonNull(marker.getTag()).toString());

        if (entry != null) {
            showReminderRemoveAlert(entry);
        }
        return true;
    }
}