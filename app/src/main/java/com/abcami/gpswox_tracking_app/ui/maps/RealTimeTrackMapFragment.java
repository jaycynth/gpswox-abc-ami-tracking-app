package com.abcami.gpswox_tracking_app.ui.maps;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abcami.gpswox_tracking_app.R;
import com.abcami.gpswox_tracking_app.data.model.Coordinate;
import com.abcami.gpswox_tracking_app.data.model.GetLatestDevices;
import com.abcami.gpswox_tracking_app.data.model.GetRoutes;
import com.abcami.gpswox_tracking_app.data.model.PolylineData;
import com.abcami.gpswox_tracking_app.data.model.Route;
import com.abcami.gpswox_tracking_app.data.model.Tail;
import com.abcami.gpswox_tracking_app.data.model.latestDevices.Item;
import com.abcami.gpswox_tracking_app.utils.ClusterMarker;
import com.abcami.gpswox_tracking_app.utils.MyClusterManagerRenderer;
import com.abcami.gpswox_tracking_app.utils.SharedPrefManager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.clustering.ClusterManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.ButterKnife;

import static com.abcami.gpswox_tracking_app.utils.Constants.LOCATION_UPDATE_INTERVAL;
import static com.abcami.gpswox_tracking_app.utils.Utils.resetMap;
import static com.abcami.gpswox_tracking_app.utils.Utils.zoomRoute;

public class RealTimeTrackMapFragment extends Fragment implements OnMapReadyCallback, ClusterManager.OnClusterItemInfoWindowClickListener<ClusterMarker>,
        GoogleMap.OnPolylineClickListener {
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;

    private MapsViewModel mapsViewModel;

    private Handler mHandler = new Handler();
    private Runnable mRunnable;

    private ClusterManager<ClusterMarker> mClusterManager;
    private MyClusterManagerRenderer mClusterManagerRenderer;
    private ArrayList<ClusterMarker> mClusterMarkers = new ArrayList<>();

    private List<LatLng> latLngList = new ArrayList<>();

    private List<Item> latestItems = new ArrayList<>();
    List<Tail> tails = new ArrayList<>();


    private String gpswoxToken;
    private LatLng currentPosition;
    private int userId;
    private ClusterMarker mSelectedMarker;

    private List<Route> routes = new ArrayList<>();

    private String routeTitle;
    private List<PolylineData> mPolylineData = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_real_time_track_map, container, false);
        ButterKnife.bind(this, view);

        mapsViewModel = new ViewModelProvider(this).get(MapsViewModel.class);

        gpswoxToken = SharedPrefManager.getInstance(getActivity()).getApiHashKey();

        if (mapFragment == null) {
            mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.realtime_map);
            mapFragment.getMapAsync(this);
        }

        mapsViewModel.getRoutes(gpswoxToken);

        mapsViewModel.getLatestDeviceResponse().observe(getViewLifecycleOwner(), getLatestDevicesState -> {

            if (getLatestDevicesState.getLatestDevices() != null) {
                getLatestDevices(getLatestDevicesState.getLatestDevices());
            }

            if (getLatestDevicesState.getMessage() != null) {
                ErrorMessage(getLatestDevicesState.getMessage());
            }

            if (getLatestDevicesState.getThrowable() != null) {
                ErrorThrowable(getLatestDevicesState.getThrowable());
            }
        });

        mapsViewModel.getRoutesResponse().observe(getViewLifecycleOwner(), getRouteState -> {
            if (getRouteState.getRouteState() != null) {
                getRoutes(getRouteState.getRouteState());
            }

            if (getRouteState.getMessage() != null) {
                ErrorMessage(getRouteState.getMessage());
            }

            if (getRouteState.getThrowable() != null) {
                ErrorThrowable(getRouteState.getThrowable());
            }
        });

        routeTitle = RealTimeTrackMapFragmentArgs.fromBundle(requireArguments()).getRouteTitle();


        return view;
    }

    private void getRoutes(GetRoutes routeState) {
        if (routeState.getStatus() == 1) {
            routes = routeState.getRoutes();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startUserLocationsRunnable();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopLocationUpdates();
    }

    private void getLatestDevices(GetLatestDevices latestDevices) {
        latestItems = latestDevices.getItems();

        Gson gson = new Gson();

        for (com.abcami.gpswox_tracking_app.data.model.latestDevices.Item item : latestItems) {
            tails = gson.fromJson(item.getTail(), new TypeToken<List<Tail>>() {
            }.getType());
        }
        for (Item item : latestItems) {
            if (item.getName().equals(routeTitle)) {
                addItemToMap(item);
            }
        }
    }

    private void addItemToMap(Item item) {
        if (mMap != null) {
            resetMap(mMap, mClusterManager, mClusterMarkers);
            if (mClusterManager == null) {
                //initialize cluster manager
                mClusterManager = new ClusterManager<>(requireActivity().getApplicationContext(), mMap);
            }

            mMap.setOnInfoWindowClickListener(mClusterManager);
            mMap.setOnMarkerClickListener(mClusterManager);

            mMap.setOnInfoWindowClickListener(mClusterManager);

            mClusterManager.setOnClusterItemInfoWindowClickListener(this);

            if (mClusterManagerRenderer == null) {
                mClusterManagerRenderer = new MyClusterManagerRenderer(
                        getActivity(),
                        mMap,
                        mClusterManager
                );
                mClusterManager.setRenderer(mClusterManagerRenderer);
            }

            try {
                String snippet = "view Route";
                int avatar;

                if (item.getOnline().equals("online")) {
                    avatar = R.drawable.ic_green;
                } else if (item.getOnline().equals("ack")) {
                    avatar = R.drawable.ic_orange;
                } else {
                    avatar = R.drawable.ic_rec;
                }
                ClusterMarker newClusterMarker = new ClusterMarker(
                        new LatLng(item.getLat(), item.getLng()),
                        item.getName(),
                        snippet,
                        avatar,
                        item.getAddress());

                mClusterManager.addItem(newClusterMarker);
                mClusterMarkers.add(newClusterMarker);

            } catch (NullPointerException e) {
                Log.e("MAP MARKERS", "addMapMarkers: NullPointerException: " + e.getMessage());
            }

            latLngList.add(new LatLng(item.getLat(), item.getLng()));
        }
        mClusterManager.cluster();
        addPolylineToMap();
    }

    private void addPolylineToMap() {
        new Handler(Looper.getMainLooper()).post(() -> {
            if (mPolylineData.size() > 0) {
                for (PolylineData polylineData : mPolylineData) {
                    polylineData.getPolyline().remove();
                }
                mPolylineData.clear();
                mPolylineData = new ArrayList<>();
            }


            for (Route route : routes) {
                if (route.getName().equals(routeTitle)) {
                    List<Coordinate> decodedPath = route.getCoordinates();
                    List<LatLng> newDecodedPath = new ArrayList<>();

                    // This loops through all the LatLng coordinates of ONE polyline.
                    for (Coordinate latLng : decodedPath) {
                        newDecodedPath.add(new LatLng(
                                latLng.getLat(),
                                latLng.getLng()
                        ));
                    }
                    com.google.android.gms.maps.model.Polyline polyline = mMap.addPolyline(new PolylineOptions().addAll(newDecodedPath));
                    polyline.setColor(ContextCompat.getColor(requireActivity(), R.color.blue));
                    polyline.setClickable(true);

                    mPolylineData.add(new PolylineData(polyline, null));

                    onPolylineClick(polyline);
                    zoomRoute(mMap, polyline.getPoints());

                }
            }
        });
    }


    /*Realtime updates of location of users on the map*/
    private void startUserLocationsRunnable() {
        Log.d("LOCATION UPDATES", "startUserLocationsRunnable: starting runnable for retrieving updated locations.");
        mHandler.postDelayed(mRunnable = () -> {
            retrieveUserLocations();
            mHandler.postDelayed(mRunnable, LOCATION_UPDATE_INTERVAL);
        }, LOCATION_UPDATE_INTERVAL);
    }


    private void retrieveUserLocations() {
        Log.d("LOCATION UPDATES", "retrieveUserLocations: retrieving location of all users");
        mapsViewModel.getLatestDevice(gpswoxToken, LOCATION_UPDATE_INTERVAL);
    }


    private void stopLocationUpdates() {
        mHandler.removeCallbacks(mRunnable);
    }

    private void ErrorMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private void ErrorThrowable(Throwable throwable) {
        if (throwable instanceof IOException) {
            Toast.makeText(getActivity(), "network issue", Toast.LENGTH_SHORT).show();
        } else {
            Log.d("CONVERSION ERROR", Objects.requireNonNull(throwable.getMessage()));
            Toast.makeText(getActivity(), "conversion error", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        mMap = googleMap;

        Toast.makeText(getActivity(), "Loading....", Toast.LENGTH_LONG).show();
        mapsViewModel.getLatestDevice(gpswoxToken, LOCATION_UPDATE_INTERVAL);

    }

    @Override
    public void onClusterItemInfoWindowClick(ClusterMarker item) {
        currentPosition = item.getPosition();
        mSelectedMarker = item;

    }

    @Override
    public void onPolylineClick(Polyline polyline) {

        for (PolylineData polylineData : mPolylineData) {

            Log.d("POLYLINE CLICK", "onPolylineClick: toString: " + polylineData.toString());
            if (polyline.getId().equals(polylineData.getPolyline().getId())) {
                polylineData.getPolyline().setColor(ContextCompat.getColor(requireActivity(), R.color.blue));
                polylineData.getPolyline().setZIndex(1);

                LatLng endLocation = new LatLng(
                        polylineData.getPolyline().getPoints().get(polylineData.getPolyline().getPoints().size()-1).latitude,
                        polylineData.getPolyline().getPoints().get(polylineData.getPolyline().getPoints().size()-1).longitude
                );

                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(endLocation)
                        .title("Destination"));

                marker.showInfoWindow();

            } else {
                polylineData.getPolyline().setColor(ContextCompat.getColor(requireActivity(), R.color.red));
                polylineData.getPolyline().setZIndex(0);
            }
        }
    }
}
