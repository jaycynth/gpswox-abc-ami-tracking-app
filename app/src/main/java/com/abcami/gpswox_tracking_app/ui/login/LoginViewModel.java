package com.abcami.gpswox_tracking_app.ui.login;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.abcami.gpswox_tracking_app.data.datastates.UserLoginState;
import com.abcami.gpswox_tracking_app.data.repositories.UserLoginRepo;

public class LoginViewModel extends AndroidViewModel {

    private MediatorLiveData<UserLoginState> userLoginMediatorLiveData;
    private UserLoginRepo userLoginRepo;


    public LoginViewModel(@NonNull Application application) {
        super(application);

        userLoginMediatorLiveData = new MediatorLiveData<>();
        userLoginRepo = new UserLoginRepo(application);
    }

    public LiveData<UserLoginState> userLoginResponse(){
        return userLoginMediatorLiveData;
    }

    public void userLogin(String email, String password){
        LiveData<UserLoginState> userLoginStateLiveData = userLoginRepo.userLogin(email, password);
        userLoginMediatorLiveData.addSource(userLoginStateLiveData,
                userLoginMediatorLiveData-> {
                    if (this.userLoginMediatorLiveData.hasActiveObservers()){
                        this.userLoginMediatorLiveData.removeSource(userLoginStateLiveData);
                    }
                    this.userLoginMediatorLiveData.setValue(userLoginMediatorLiveData);
                });
    }
}
