package com.abcami.gpswox_tracking_app.ui.maps;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.abcami.gpswox_tracking_app.data.datastates.AddGeofenceState;
import com.abcami.gpswox_tracking_app.data.datastates.AddRouteState;
import com.abcami.gpswox_tracking_app.data.datastates.CalculateDirectionState;
import com.abcami.gpswox_tracking_app.data.datastates.DestroyGeofenceState;
import com.abcami.gpswox_tracking_app.data.datastates.DestroyRouteState;
import com.abcami.gpswox_tracking_app.data.datastates.GeocodeAddressState;
import com.abcami.gpswox_tracking_app.data.datastates.GetDevicesState;
import com.abcami.gpswox_tracking_app.data.datastates.GetGeofencesState;
import com.abcami.gpswox_tracking_app.data.datastates.GetLatestDevicesState;
import com.abcami.gpswox_tracking_app.data.datastates.GetRouteState;
import com.abcami.gpswox_tracking_app.data.repositories.AddGeofenceRepo;
import com.abcami.gpswox_tracking_app.data.repositories.AddRouteRepo;
import com.abcami.gpswox_tracking_app.data.repositories.CalculateDirectionsRepo;
import com.abcami.gpswox_tracking_app.data.repositories.DestroyGeofenceRepo;
import com.abcami.gpswox_tracking_app.data.repositories.DestroyRouteRepo;
import com.abcami.gpswox_tracking_app.data.repositories.GeocodeAddressRepo;
import com.abcami.gpswox_tracking_app.data.repositories.GetDevicesRepo;
import com.abcami.gpswox_tracking_app.data.repositories.GetGeofencesRepo;
import com.abcami.gpswox_tracking_app.data.repositories.GetLatestDeviceRepo;
import com.abcami.gpswox_tracking_app.data.repositories.GetRoutesRepo;
import com.google.android.gms.maps.model.LatLng;


public class MapsViewModel extends AndroidViewModel {

    private MediatorLiveData<GetDevicesState> getDevicesStateMediatorLiveData;
    private GetDevicesRepo getDevicesRepo;

    private MediatorLiveData<AddGeofenceState> addGeofenceStateMediatorLiveData;
    private AddGeofenceRepo addGeofenceRepo;

    private MediatorLiveData<GetGeofencesState> getGeofenceStateMediatorLiveData;
    private GetGeofencesRepo getGeofencesRepo;

    private MediatorLiveData<DestroyGeofenceState> destroyGeofenceStateMediatorLiveData;
    private DestroyGeofenceRepo destroyGeofenceRepo;

    private MediatorLiveData<AddRouteState> addRouteStateMediatorLiveData;
    private AddRouteRepo addRouteRepo;

    private MediatorLiveData<GetRouteState> getRouteStateMediatorLiveData;
    private GetRoutesRepo getRoutesRepo;

    private MediatorLiveData<DestroyRouteState> destroyRouteStateMediatorLiveData;
    private DestroyRouteRepo destroyRouteRepo;

    private MediatorLiveData<GetLatestDevicesState> getLatestDevicesStateMediatorLiveData;
    private GetLatestDeviceRepo getLatestDeviceRepo;

    private MediatorLiveData<GeocodeAddressState> geocodeAddressStateMediatorLiveData;
    private GeocodeAddressRepo geocodeAddressRepo;

    private MediatorLiveData<CalculateDirectionState> calculateDirectionStateMediatorLiveData;
    private CalculateDirectionsRepo calculateDirectionsRepo;

    // private RouteRepo routeRepo;

    public MapsViewModel(@NonNull Application application) {
        super(application);

        getDevicesStateMediatorLiveData = new MediatorLiveData<>();
        getDevicesRepo = new GetDevicesRepo(application);

        addGeofenceStateMediatorLiveData = new MediatorLiveData<>();
        addGeofenceRepo = new AddGeofenceRepo(application);

        getGeofenceStateMediatorLiveData = new MediatorLiveData<>();
        getGeofencesRepo = new GetGeofencesRepo(application);

        destroyGeofenceStateMediatorLiveData = new MediatorLiveData<>();
        destroyGeofenceRepo = new DestroyGeofenceRepo(application);

        addRouteStateMediatorLiveData = new MediatorLiveData<>();
        addRouteRepo = new AddRouteRepo(application);

        getRouteStateMediatorLiveData = new MediatorLiveData<>();
        getRoutesRepo = new GetRoutesRepo(application);

        destroyRouteStateMediatorLiveData = new MediatorLiveData<>();
        destroyRouteRepo = new DestroyRouteRepo(application);

        getLatestDevicesStateMediatorLiveData = new MediatorLiveData<>();
        getLatestDeviceRepo = new GetLatestDeviceRepo(application);

        geocodeAddressStateMediatorLiveData = new MediatorLiveData<>();
        geocodeAddressRepo = new GeocodeAddressRepo(application);

        calculateDirectionStateMediatorLiveData = new MediatorLiveData<>();
        calculateDirectionsRepo = new CalculateDirectionsRepo(application);

        //routeRepo = new RouteRepo();
    }

    public LiveData<GetDevicesState> getDevicesResponse() {
        return getDevicesStateMediatorLiveData;
    }

    public void getDevices(String api_hash_key) {
        LiveData<GetDevicesState> getDevicesStateLiveData = getDevicesRepo.getDevices(api_hash_key);
        getDevicesStateMediatorLiveData.addSource(getDevicesStateLiveData,
                getDevicesStateMediatorLiveData -> {
                    if (this.getDevicesStateMediatorLiveData.hasActiveObservers()) {
                        this.getDevicesStateMediatorLiveData.removeSource(getDevicesStateLiveData);
                    }
                    this.getDevicesStateMediatorLiveData.setValue(getDevicesStateMediatorLiveData);
                });
    }


    public LiveData<AddGeofenceState> addGeofenceResponse() {
        return addGeofenceStateMediatorLiveData;
    }

    public void addGeofence(String user_api_hash, String type, String name, String polygonColor, int radius,
                            LatLng center) {
        LiveData<AddGeofenceState> addGeofenceStateLiveData = addGeofenceRepo.addGeofence(user_api_hash, type, name, polygonColor, radius, center);
        addGeofenceStateMediatorLiveData.addSource(addGeofenceStateLiveData,
                addGeofenceStateMediatorLiveData -> {
                    if (this.addGeofenceStateMediatorLiveData.hasActiveObservers()) {
                        this.addGeofenceStateMediatorLiveData.removeSource(addGeofenceStateLiveData);
                    }
                    this.addGeofenceStateMediatorLiveData.setValue(addGeofenceStateMediatorLiveData);
                });
    }


    public LiveData<GetGeofencesState> getGeofencesResponse() {
        return getGeofenceStateMediatorLiveData;
    }

    public void getGeofences(String api_hash_key) {
        LiveData<GetGeofencesState> getGeofencesStateLiveData = getGeofencesRepo.getGeosfences(api_hash_key);
        getGeofenceStateMediatorLiveData.addSource(getGeofencesStateLiveData,
                getGeofenceStateMediatorLiveData -> {
                    if (this.getGeofenceStateMediatorLiveData.hasActiveObservers()) {
                        this.getGeofenceStateMediatorLiveData.removeSource(getGeofencesStateLiveData);
                    }
                    this.getGeofenceStateMediatorLiveData.setValue(getGeofenceStateMediatorLiveData);
                });
    }

    public LiveData<DestroyGeofenceState> destroyGeofenceResponse() {
        return destroyGeofenceStateMediatorLiveData;
    }

    public void destroyGeofence(String api_hash_key, int geofence_id) {
        LiveData<DestroyGeofenceState> destroyGeofenceStateLiveData = destroyGeofenceRepo.destroyGeofence(api_hash_key, geofence_id);
        destroyGeofenceStateMediatorLiveData.addSource(destroyGeofenceStateLiveData,
                destroyGeofenceStateMediatorLiveData -> {
                    if (this.destroyGeofenceStateMediatorLiveData.hasActiveObservers()) {
                        this.destroyGeofenceStateMediatorLiveData.removeSource(destroyGeofenceStateLiveData);
                    }
                    this.destroyGeofenceStateMediatorLiveData.setValue(destroyGeofenceStateMediatorLiveData);
                });
    }

    public LiveData<AddRouteState> addRouteResponse() {
        return addRouteStateMediatorLiveData;
    }

    public void addRoute(String key, String name, String color, String polylines) {
        LiveData<AddRouteState> addRouteStateLiveData = addRouteRepo.addRoute(key, name, color, polylines);

        addRouteStateMediatorLiveData.addSource(addRouteStateLiveData,
                addRouteStateMediatorLiveData -> {
                    if (this.addRouteStateMediatorLiveData.hasActiveObservers()) {
                        this.addRouteStateMediatorLiveData.removeSource(addRouteStateLiveData);
                    }
                    this.addRouteStateMediatorLiveData.setValue(addRouteStateMediatorLiveData);
                });
    }

    public LiveData<GetRouteState> getRoutesResponse() {
        return getRouteStateMediatorLiveData;
    }

    public void getRoutes(String key) {
        LiveData<GetRouteState> getRouteStateLiveData = getRoutesRepo.getRoutes(key);
        getRouteStateMediatorLiveData.addSource(getRouteStateLiveData,
                getRouteStateMediatorLiveData -> {
                    if (this.getRouteStateMediatorLiveData.hasActiveObservers()) {
                        this.getRouteStateMediatorLiveData.removeSource(getRouteStateLiveData);
                    }
                    this.getRouteStateMediatorLiveData.setValue(getRouteStateMediatorLiveData);
                });
    }

    public LiveData<DestroyRouteState> destroyRouteResponse() {
        return destroyRouteStateMediatorLiveData;
    }

    public void destroyRoute(String key, int routeId) {
        LiveData<DestroyRouteState> destroyRouteStateLiveData = destroyRouteRepo.destroyRoute(key, routeId);
        destroyRouteStateMediatorLiveData.addSource(destroyRouteStateLiveData,
                destroyRouteStateMediatorLiveData -> {
                    if (this.destroyRouteStateMediatorLiveData.hasActiveObservers()) {
                        this.destroyRouteStateMediatorLiveData.removeSource(destroyRouteStateLiveData);
                    }
                    this.destroyRouteStateMediatorLiveData.setValue(destroyRouteStateMediatorLiveData);
                });
    }


    public LiveData<GetLatestDevicesState> getLatestDeviceResponse() {
        return getLatestDevicesStateMediatorLiveData;
    }

    public void getLatestDevice(String key, int time) {
        LiveData<GetLatestDevicesState> getLatestDevicesStateLiveData = getLatestDeviceRepo.getLatestDevics(key, time);
        getLatestDevicesStateMediatorLiveData.addSource(getLatestDevicesStateLiveData,
                getLatestDevicesStateMediatorLiveData -> {
                    if (this.getLatestDevicesStateMediatorLiveData.hasActiveObservers()) {
                        this.getLatestDevicesStateMediatorLiveData.removeSource(getLatestDevicesStateLiveData);
                    }
                    this.getLatestDevicesStateMediatorLiveData.setValue(getLatestDevicesStateMediatorLiveData);
                });
    }

    public LiveData<GeocodeAddressState> getGeocodeResponse() {
        return geocodeAddressStateMediatorLiveData;
    }

    public void geocodeAddress(double lat, double lng) {
        LiveData<GeocodeAddressState> geocodeAddressStateLiveData = geocodeAddressRepo.geocodeAddress(lat, lng);
        geocodeAddressStateMediatorLiveData.addSource(geocodeAddressStateLiveData,
                geocodeAddressStateMediatorLiveData -> {
                    if (this.geocodeAddressStateMediatorLiveData.hasActiveObservers()) {
                        this.geocodeAddressStateMediatorLiveData.removeSource(geocodeAddressStateLiveData);
                    }
                    this.geocodeAddressStateMediatorLiveData.setValue(geocodeAddressStateMediatorLiveData);
                });
    }

    public LiveData<CalculateDirectionState> calculateDirectionsResponse() {
        return calculateDirectionStateMediatorLiveData;
    }

    public void calculateDirections(String from, String to) {
        LiveData<CalculateDirectionState> calculateDirectionStateLiveData = calculateDirectionsRepo.calculateDirections(from, to);
        calculateDirectionStateMediatorLiveData.addSource(calculateDirectionStateLiveData,
                calculateDirectionStateMediatorLiveData -> {
                    if (this.calculateDirectionStateMediatorLiveData.hasActiveObservers()) {
                        this.calculateDirectionStateMediatorLiveData.removeSource(calculateDirectionStateLiveData);
                    }
                    this.calculateDirectionStateMediatorLiveData.setValue(calculateDirectionStateMediatorLiveData);
                });
    }
//    public void insertRoute(AddedRoute addedRoute) {
//        routeRepo.saveRoute(addedRoute);
//    }
//
//    public void deleteRoute(AddedRoute addedRoute) {
//        routeRepo.deleteRoute(addedRoute);
//    }
//
//    public void getAllRoutes() {
//        routeRepo.getAllRoutes();
//    }
}
