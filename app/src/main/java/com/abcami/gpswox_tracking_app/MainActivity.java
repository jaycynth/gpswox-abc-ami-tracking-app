package com.abcami.gpswox_tracking_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;

import android.os.Bundle;

import java.util.Objects;

import static androidx.navigation.Navigation.findNavController;
import static androidx.navigation.ui.NavigationUI.navigateUp;
import static androidx.navigation.ui.NavigationUI.setupActionBarWithNavController;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
         appBarConfiguration =
                new AppBarConfiguration.Builder(navController.getGraph()).build();


        setupActionBarWithNavController(this, navController, appBarConfiguration);

    }


    @Override
    public boolean onSupportNavigateUp() {
        return navigateUp(findNavController(this, R.id.nav_host_fragment), appBarConfiguration);
    }
}