package com.abcami.gpswox_tracking_app.api;

import com.abcami.gpswox_tracking_app.data.model.AddGeofence;
import com.abcami.gpswox_tracking_app.data.model.AddRoute;
import com.abcami.gpswox_tracking_app.data.model.CalculateDirections;
import com.abcami.gpswox_tracking_app.data.model.Coordinate;
import com.abcami.gpswox_tracking_app.data.model.DestroyGeofence;
import com.abcami.gpswox_tracking_app.data.model.DestroyRoute;
import com.abcami.gpswox_tracking_app.data.model.GeocodeAddress;
import com.abcami.gpswox_tracking_app.data.model.GetDevices;
import com.abcami.gpswox_tracking_app.data.model.GetGeofences;
import com.abcami.gpswox_tracking_app.data.model.GetLatestDevices;
import com.abcami.gpswox_tracking_app.data.model.GetRoutes;
import com.abcami.gpswox_tracking_app.data.model.UserLogin;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

    @Headers({"Accept: application/json"})
    @POST("login")
    Call<UserLogin> userLogin(
            @Query("email") String email,
            @Query("password") String password
    );


    @Headers({"Accept: application/json"})
    @GET("get_devices")
    Call<List<GetDevices>> getDevices(
            @Query("user_api_hash") String user_api_hash
    );


    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("add_geofence")
    Call<AddGeofence> addGeofence(
            @Query("user_api_hash") String user_api_hash,
            @Field("type") String type,
            @Field("name") String name,
            @Field("polygon_color") String polygon_color,
            @Field("radius") int radius,
            @Field("center") LatLng center
    );

    @Headers({"Accept: application/json"})
    @GET("get_geofences")
    Call<GetGeofences> getGoefences(
            @Query("user_api_hash") String user_api_hash
    );

    @Headers({"Accept: application/json"})
    @GET("destroy_geofence")
    Call<DestroyGeofence> destroyGeofence(
            @Query("user_api_hash") String user_api_hash,
            @Query("geofence_id") int geofence_id
    );


    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("add_route")
    Call<AddRoute> addRoute(
            @Query("user_api_hash") String user_api_hash,
            @Field("name") String name,
            @Field("color") String color,
            @Field("polyline") String polylines
    );

    @Headers({"Accept: application/json"})
    @GET("get_routes")
    Call<GetRoutes> getRoutes(
            @Query("user_api_hash") String user_api_hash

    );

    @Headers({"Accept: application/json"})
    @GET("destroy_route")
    Call<DestroyRoute> destroyRoute(
            @Query("user_api_hash") String user_api_hash,
            @Query("route_id") int route_id

    );

    @Headers({"Accept: application/json"})
    @GET("get_devices_latest")
    Call<GetLatestDevices> getLatestDevices(
            @Query("user_api_hash") String user_api_hash,
            @Query("time") int time

    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("geocodeCoordinates")
    Call<GeocodeAddress> geocodeAddress(
            @Field("lat") double lat,
            @Field("lng") double lng
    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("directions")
    Call<CalculateDirections> calculateDirections(
            @Field("from") String fromAddress,
            @Field("to") String toAddress
    );




}
