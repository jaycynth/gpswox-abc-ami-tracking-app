package com.abcami.gpswox_tracking_app.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.abcami.gpswox_tracking_app.api.ApiEndpoints.ABC_BASE_URL;
import static com.abcami.gpswox_tracking_app.api.ApiEndpoints.BASE_URL;

public class ApiClient {
    private Context context;


    public ApiClient(Context context) {
        this.context = context;
    }


    //Configure OkHttpClient
    private OkHttpClient.Builder okHttpClient() {


        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();

        okHttpClient.connectTimeout(2, TimeUnit.MINUTES);
        okHttpClient.readTimeout(7, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(2, TimeUnit.MINUTES);



         HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.addInterceptor(logging);



        return okHttpClient;
    }


    private Retrofit getClient() {


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        OkHttpClient.Builder okhttpBuilder = okHttpClient();
        builder.client(okhttpBuilder.build());

        return builder.build();
    }

    private Retrofit getAbcClient() {


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(ABC_BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        OkHttpClient.Builder okhttpBuilder = okHttpClient();
        builder.client(okhttpBuilder.build());

        return builder.build();
    }


    public ApiService amiService() {
        return getClient().create(ApiService.class);
    }

    public ApiService abcService() { return  getAbcClient().create(ApiService.class);}


}
