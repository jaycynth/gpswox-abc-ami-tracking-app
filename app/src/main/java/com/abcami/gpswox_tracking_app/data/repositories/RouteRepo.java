package com.abcami.gpswox_tracking_app.data.repositories;

import com.abcami.gpswox_tracking_app.data.model.AddedRoute;
import com.abcami.gpswox_tracking_app.database.TrackingDatabase;
import com.abcami.gpswox_tracking_app.database.dao.RouteDao;
import com.abcami.gpswox_tracking_app.utils.TrackerApp;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class RouteRepo {

    private RouteDao routeDao;
    private Executor executor;

    public RouteRepo() {
        executor = Executors.newSingleThreadExecutor();
        routeDao = TrackingDatabase.getDatabase(TrackerApp.context).routeDao();
    }

    public void saveRoute(AddedRoute addedRoute) {
        executor.execute(() -> routeDao.insert(addedRoute));
    }


    public void deleteRoute(AddedRoute addedRoute) {
        executor.execute(() -> routeDao.delete(addedRoute));
    }

    public void getAllRoutes(){
        executor.execute(()->routeDao.getAllRoutes());
    }
}
