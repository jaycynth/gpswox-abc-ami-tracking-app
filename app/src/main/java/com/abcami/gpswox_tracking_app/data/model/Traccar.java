package com.abcami.gpswox_tracking_app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Traccar {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("uniqueId")
    @Expose
    private String uniqueId;
    @SerializedName("latestPosition_id")
    @Expose
    private Integer latestPositionId;
    @SerializedName("lastValidLatitude")
    @Expose
    private Double lastValidLatitude;
    @SerializedName("lastValidLongitude")
    @Expose
    private Double lastValidLongitude;
    @SerializedName("other")
    @Expose
    private String other;
    @SerializedName("speed")
    @Expose
    private String speed;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("device_time")
    @Expose
    private String deviceTime;
    @SerializedName("server_time")
    @Expose
    private String serverTime;
    @SerializedName("ack_time")
    @Expose
    private String ackTime;
    @SerializedName("altitude")
    @Expose
    private double altitude;
    @SerializedName("course")
    @Expose
    private double course;
    @SerializedName("power")
    @Expose
    private Object power;
    @SerializedName("address")
    @Expose
    private Object address;
    @SerializedName("protocol")
    @Expose
    private String protocol;
    @SerializedName("latest_positions")
    @Expose
    private String latestPositions;
    @SerializedName("moved_at")
    @Expose
    private String movedAt;
    @SerializedName("stoped_at")
    @Expose
    private String stopedAt;
    @SerializedName("engine_on_at")
    @Expose
    private String engineOnAt;
    @SerializedName("engine_off_at")
    @Expose
    private String engineOffAt;
    @SerializedName("engine_changed_at")
    @Expose
    private String engineChangedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Integer getLatestPositionId() {
        return latestPositionId;
    }

    public void setLatestPositionId(Integer latestPositionId) {
        this.latestPositionId = latestPositionId;
    }

    public Double getLastValidLatitude() {
        return lastValidLatitude;
    }

    public void setLastValidLatitude(Double lastValidLatitude) {
        this.lastValidLatitude = lastValidLatitude;
    }

    public Double getLastValidLongitude() {
        return lastValidLongitude;
    }

    public void setLastValidLongitude(Double lastValidLongitude) {
        this.lastValidLongitude = lastValidLongitude;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDeviceTime() {
        return deviceTime;
    }

    public void setDeviceTime(String deviceTime) {
        this.deviceTime = deviceTime;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getAckTime() {
        return ackTime;
    }

    public void setAckTime(String ackTime) {
        this.ackTime = ackTime;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double
    getCourse() {
        return course;
    }

    public void setCourse(double course) {
        this.course = course;
    }

    public Object getPower() {
        return power;
    }

    public void setPower(Object power) {
        this.power = power;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getLatestPositions() {
        return latestPositions;
    }

    public void setLatestPositions(String latestPositions) {
        this.latestPositions = latestPositions;
    }

    public String getMovedAt() {
        return movedAt;
    }

    public void setMovedAt(String movedAt) {
        this.movedAt = movedAt;
    }

    public String getStopedAt() {
        return stopedAt;
    }

    public void setStopedAt(String stopedAt) {
        this.stopedAt = stopedAt;
    }

    public String getEngineOnAt() {
        return engineOnAt;
    }

    public void setEngineOnAt(String engineOnAt) {
        this.engineOnAt = engineOnAt;
    }

    public String getEngineOffAt() {
        return engineOffAt;
    }

    public void setEngineOffAt(String engineOffAt) {
        this.engineOffAt = engineOffAt;
    }

    public String getEngineChangedAt() {
        return engineChangedAt;
    }

    public void setEngineChangedAt(String engineChangedAt) {
        this.engineChangedAt = engineChangedAt;
    }
}
