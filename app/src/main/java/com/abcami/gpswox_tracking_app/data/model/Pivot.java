package com.abcami.gpswox_tracking_app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pivot {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("device_id")
    @Expose
    private Integer deviceId;
    @SerializedName("group_id")
    @Expose
    private Integer groupId;
    @SerializedName("current_driver_id")
    @Expose
    private Object currentDriverId;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("timezone_id")
    @Expose
    private Object timezoneId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Object getCurrentDriverId() {
        return currentDriverId;
    }

    public void setCurrentDriverId(Object currentDriverId) {
        this.currentDriverId = currentDriverId;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Object getTimezoneId() {
        return timezoneId;
    }

    public void setTimezoneId(Object timezoneId) {
        this.timezoneId = timezoneId;
    }
}

