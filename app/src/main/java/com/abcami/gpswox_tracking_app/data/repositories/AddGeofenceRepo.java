package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.AddGeofenceState;
import com.abcami.gpswox_tracking_app.data.model.AddGeofence;
import com.abcami.gpswox_tracking_app.data.model.AddGeofenceErrors;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddGeofenceRepo {

    private ApiClient mApiClient;

    public AddGeofenceRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<AddGeofenceState> addGeofence(String user_api_hash, String type, String name, String polygonColor,
                                                  int radius, LatLng center) {

        final MutableLiveData<AddGeofenceState> addGeofenceStateMutableLiveData = new MutableLiveData<>();

        Call<AddGeofence> call = mApiClient.amiService().addGeofence(user_api_hash, type, name, polygonColor, radius, center);
        call.enqueue(new Callback<AddGeofence>() {
            @Override
            public void onResponse(Call<AddGeofence> call, Response<AddGeofence> response) {
                if (response.code() == 200) {
                    addGeofenceStateMutableLiveData.setValue(new AddGeofenceState(response.body()));
                } else if (response.code() == 400) {
                    Gson gson = new Gson();

                    Type type = new TypeToken<AddGeofenceErrors>() {
                    }.getType();
                    AddGeofenceErrors addGeofenceErrors = gson.fromJson(response.errorBody().charStream(), type);
                    addGeofenceStateMutableLiveData.setValue(new AddGeofenceState(addGeofenceErrors));
                }else{
                    addGeofenceStateMutableLiveData.setValue(new AddGeofenceState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AddGeofence> call, Throwable t) {
                addGeofenceStateMutableLiveData.setValue(new AddGeofenceState(t));
            }
        });

        return addGeofenceStateMutableLiveData;
    }
}
