package com.abcami.gpswox_tracking_app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Errors {
    @SerializedName("center")
    @Expose
    private List<String> center = null;
    @SerializedName("polygon_color")
    @Expose
    private List<String> polygonColor = null;

    public List<String> getCenter() {
        return center;
    }

    public void setCenter(List<String> center) {
        this.center = center;
    }

    public List<String> getPolygonColor() {
        return polygonColor;
    }

    public void setPolygonColor(List<String> polygonColor) {
        this.polygonColor = polygonColor;
    }
}
