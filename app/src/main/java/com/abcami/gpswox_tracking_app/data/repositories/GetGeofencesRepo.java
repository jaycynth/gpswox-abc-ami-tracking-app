package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.GetGeofencesState;
import com.abcami.gpswox_tracking_app.data.model.GetGeofences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetGeofencesRepo {

    private ApiClient mApiClient;

    public GetGeofencesRepo(Application application) {
        mApiClient = new ApiClient(application);
    }


    public LiveData<GetGeofencesState> getGeosfences(String user_api_hash) {
        final MutableLiveData<GetGeofencesState> getGeofenceStateMutableLiveData = new MutableLiveData<>();

        Call<GetGeofences> call = mApiClient.amiService().getGoefences(user_api_hash);
        call.enqueue(new Callback<GetGeofences>() {
            @Override
            public void onResponse(Call<GetGeofences> call, Response<GetGeofences> response) {
                if (response.code() == 200) {
                    getGeofenceStateMutableLiveData.setValue(new GetGeofencesState(response.body()));
                } else {
                    getGeofenceStateMutableLiveData.setValue(new GetGeofencesState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GetGeofences> call, Throwable t) {
                getGeofenceStateMutableLiveData.setValue(new GetGeofencesState(t));
            }
        });

        return getGeofenceStateMutableLiveData;
    }
}
