package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.GetDevicesState;
import com.abcami.gpswox_tracking_app.data.model.GetDevices;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetDevicesRepo {
    
    private ApiClient mApiClient;

    public GetDevicesRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<GetDevicesState> getDevices(String api_hash_key) {
        final MutableLiveData<GetDevicesState> getDevicesStateMutableLiveData = new MutableLiveData<>();
        Call<List<GetDevices>> call = mApiClient.amiService().getDevices(api_hash_key);
        call.enqueue(new Callback<List<GetDevices>>() {
            @Override
            public void onResponse(Call<List<GetDevices>> call, Response<List<GetDevices>> response) {
                if (response.code() == 200) {
                    getDevicesStateMutableLiveData.setValue(new GetDevicesState(response.body()));

                }
                else {
                    getDevicesStateMutableLiveData.setValue(new GetDevicesState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<List<GetDevices>> call, Throwable t) {
                getDevicesStateMutableLiveData.setValue(new GetDevicesState(t));
            }
        });

        return getDevicesStateMutableLiveData;


    }
    
}
