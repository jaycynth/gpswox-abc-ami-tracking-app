package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.CalculateDirectionState;
import com.abcami.gpswox_tracking_app.data.model.CalculateDirections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalculateDirectionsRepo {
    private ApiClient apiClient;

    public CalculateDirectionsRepo(Application application) {
        apiClient = new ApiClient(application);
    }

    public LiveData<CalculateDirectionState> calculateDirections(String from, String to){
        final MutableLiveData<CalculateDirectionState> calculateDirectionStateMutableLiveData = new MutableLiveData<>();
        Call<CalculateDirections> call = apiClient.abcService().calculateDirections(from,to);
        call.enqueue(new Callback<CalculateDirections>() {
            @Override
            public void onResponse(Call<CalculateDirections> call, Response<CalculateDirections> response) {
                if(response.code() == 200){
                    calculateDirectionStateMutableLiveData.setValue(new CalculateDirectionState(response.body()));
                }else{
                    calculateDirectionStateMutableLiveData.setValue(new CalculateDirectionState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<CalculateDirections> call, Throwable t) {
              calculateDirectionStateMutableLiveData.setValue(new CalculateDirectionState(t));
            }
        });
        return calculateDirectionStateMutableLiveData;
    }
}
