package com.abcami.gpswox_tracking_app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeviceData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("current_driver_id")
    @Expose
    private Object currentDriverId;
    @SerializedName("timezone_id")
    @Expose
    private Object timezoneId;
    @SerializedName("traccar_device_id")
    @Expose
    private Integer traccarDeviceId;
    @SerializedName("icon_id")
    @Expose
    private Integer iconId;
    @SerializedName("icon_colors")
    @Expose
    private IconColors_ iconColors;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("deleted")
    @Expose
    private Integer deleted;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("fuel_measurement_id")
    @Expose
    private Integer fuelMeasurementId;
    @SerializedName("fuel_quantity")
    @Expose
    private String fuelQuantity;
    @SerializedName("fuel_price")
    @Expose
    private String fuelPrice;
    @SerializedName("fuel_per_km")
    @Expose
    private String fuelPerKm;
    @SerializedName("sim_number")
    @Expose
    private String simNumber;
    @SerializedName("msisdn")
    @Expose
    private Object msisdn;
    @SerializedName("current_geofences")
    @Expose
    private Object currentGeofences;
    @SerializedName("device_model")
    @Expose
    private String deviceModel;
    @SerializedName("plate_number")
    @Expose
    private String plateNumber;
    @SerializedName("vin")
    @Expose
    private String vin;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("object_owner")
    @Expose
    private String objectOwner;
    @SerializedName("additional_notes")
    @Expose
    private String additionalNotes;
    @SerializedName("expiration_date")
    @Expose
    private Object expirationDate;
    @SerializedName("sim_expiration_date")
    @Expose
    private String simExpirationDate;
    @SerializedName("sim_activation_date")
    @Expose
    private String simActivationDate;
    @SerializedName("installation_date")
    @Expose
    private String installationDate;
    @SerializedName("tail_color")
    @Expose
    private String tailColor;
    @SerializedName("tail_length")
    @Expose
    private Integer tailLength;
    @SerializedName("engine_hours")
    @Expose
    private String engineHours;
    @SerializedName("detect_engine")
    @Expose
    private String detectEngine;
    @SerializedName("min_moving_speed")
    @Expose
    private Integer minMovingSpeed;
    @SerializedName("min_fuel_fillings")
    @Expose
    private Integer minFuelFillings;
    @SerializedName("min_fuel_thefts")
    @Expose
    private Integer minFuelThefts;
    @SerializedName("snap_to_road")
    @Expose
    private Integer snapToRoad;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("gprs_templates_only")
    @Expose
    private Integer gprsTemplatesOnly;
    @SerializedName("valid_by_avg_speed")
    @Expose
    private Integer validByAvgSpeed;
    @SerializedName("parameters")
    @Expose
    private String parameters;
    @SerializedName("currents")
    @Expose
    private Object currents;
    @SerializedName("forward")
    @Expose
    private Object forward;
    @SerializedName("stop_duration")
    @Expose
    private String stopDuration;
    @SerializedName("pivot")
    @Expose
    private Pivot pivot;
    @SerializedName("traccar")
    @Expose
    private Traccar traccar;
    @SerializedName("icon")
    @Expose
    private Icon_ icon;
    @SerializedName("sensors")
    @Expose
    private List<Object> sensors = null;
    @SerializedName("services")
    @Expose
    private List<Object> services = null;
    @SerializedName("driver")
    @Expose
    private Object driver;
    @SerializedName("users")
    @Expose
    private List<User> users = null;
    @SerializedName("lastValidLatitude")
    @Expose
    private Double lastValidLatitude;
    @SerializedName("lastValidLongitude")
    @Expose
    private Double lastValidLongitude;
    @SerializedName("latest_positions")
    @Expose
    private String latestPositions;
    @SerializedName("icon_type")
    @Expose
    private String iconType;
    @SerializedName("group_id")
    @Expose
    private Integer groupId;
    @SerializedName("user_timezone_id")
    @Expose
    private Object userTimezoneId;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("course")
    @Expose
    private Integer course;
    @SerializedName("speed")
    @Expose
    private Integer speed;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Object getCurrentDriverId() {
        return currentDriverId;
    }

    public void setCurrentDriverId(Object currentDriverId) {
        this.currentDriverId = currentDriverId;
    }

    public Object getTimezoneId() {
        return timezoneId;
    }

    public void setTimezoneId(Object timezoneId) {
        this.timezoneId = timezoneId;
    }

    public Integer getTraccarDeviceId() {
        return traccarDeviceId;
    }

    public void setTraccarDeviceId(Integer traccarDeviceId) {
        this.traccarDeviceId = traccarDeviceId;
    }

    public Integer getIconId() {
        return iconId;
    }

    public void setIconId(Integer iconId) {
        this.iconId = iconId;
    }

    public IconColors_ getIconColors() {
        return iconColors;
    }

    public void setIconColors(IconColors_ iconColors) {
        this.iconColors = iconColors;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getFuelMeasurementId() {
        return fuelMeasurementId;
    }

    public void setFuelMeasurementId(Integer fuelMeasurementId) {
        this.fuelMeasurementId = fuelMeasurementId;
    }

    public String getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(String fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getFuelPerKm() {
        return fuelPerKm;
    }

    public void setFuelPerKm(String fuelPerKm) {
        this.fuelPerKm = fuelPerKm;
    }

    public String getSimNumber() {
        return simNumber;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public Object getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(Object msisdn) {
        this.msisdn = msisdn;
    }

    public Object getCurrentGeofences() {
        return currentGeofences;
    }

    public void setCurrentGeofences(Object currentGeofences) {
        this.currentGeofences = currentGeofences;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getObjectOwner() {
        return objectOwner;
    }

    public void setObjectOwner(String objectOwner) {
        this.objectOwner = objectOwner;
    }

    public String getAdditionalNotes() {
        return additionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    public Object getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Object expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getSimExpirationDate() {
        return simExpirationDate;
    }

    public void setSimExpirationDate(String simExpirationDate) {
        this.simExpirationDate = simExpirationDate;
    }

    public String getSimActivationDate() {
        return simActivationDate;
    }

    public void setSimActivationDate(String simActivationDate) {
        this.simActivationDate = simActivationDate;
    }

    public String getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(String installationDate) {
        this.installationDate = installationDate;
    }

    public String getTailColor() {
        return tailColor;
    }

    public void setTailColor(String tailColor) {
        this.tailColor = tailColor;
    }

    public Integer getTailLength() {
        return tailLength;
    }

    public void setTailLength(Integer tailLength) {
        this.tailLength = tailLength;
    }

    public String getEngineHours() {
        return engineHours;
    }

    public void setEngineHours(String engineHours) {
        this.engineHours = engineHours;
    }

    public String getDetectEngine() {
        return detectEngine;
    }

    public void setDetectEngine(String detectEngine) {
        this.detectEngine = detectEngine;
    }

    public Integer getMinMovingSpeed() {
        return minMovingSpeed;
    }

    public void setMinMovingSpeed(Integer minMovingSpeed) {
        this.minMovingSpeed = minMovingSpeed;
    }

    public Integer getMinFuelFillings() {
        return minFuelFillings;
    }

    public void setMinFuelFillings(Integer minFuelFillings) {
        this.minFuelFillings = minFuelFillings;
    }

    public Integer getMinFuelThefts() {
        return minFuelThefts;
    }

    public void setMinFuelThefts(Integer minFuelThefts) {
        this.minFuelThefts = minFuelThefts;
    }

    public Integer getSnapToRoad() {
        return snapToRoad;
    }

    public void setSnapToRoad(Integer snapToRoad) {
        this.snapToRoad = snapToRoad;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getGprsTemplatesOnly() {
        return gprsTemplatesOnly;
    }

    public void setGprsTemplatesOnly(Integer gprsTemplatesOnly) {
        this.gprsTemplatesOnly = gprsTemplatesOnly;
    }

    public Integer getValidByAvgSpeed() {
        return validByAvgSpeed;
    }

    public void setValidByAvgSpeed(Integer validByAvgSpeed) {
        this.validByAvgSpeed = validByAvgSpeed;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public Object getCurrents() {
        return currents;
    }

    public void setCurrents(Object currents) {
        this.currents = currents;
    }

    public Object getForward() {
        return forward;
    }

    public void setForward(Object forward) {
        this.forward = forward;
    }

    public String getStopDuration() {
        return stopDuration;
    }

    public void setStopDuration(String stopDuration) {
        this.stopDuration = stopDuration;
    }

    public Pivot getPivot() {
        return pivot;
    }

    public void setPivot(Pivot pivot) {
        this.pivot = pivot;
    }

    public Traccar getTraccar() {
        return traccar;
    }

    public void setTraccar(Traccar traccar) {
        this.traccar = traccar;
    }

    public Icon_ getIcon() {
        return icon;
    }

    public void setIcon(Icon_ icon) {
        this.icon = icon;
    }

    public List<Object> getSensors() {
        return sensors;
    }

    public void setSensors(List<Object> sensors) {
        this.sensors = sensors;
    }

    public List<Object> getServices() {
        return services;
    }

    public void setServices(List<Object> services) {
        this.services = services;
    }

    public Object getDriver() {
        return driver;
    }

    public void setDriver(Object driver) {
        this.driver = driver;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Double getLastValidLatitude() {
        return lastValidLatitude;
    }

    public void setLastValidLatitude(Double lastValidLatitude) {
        this.lastValidLatitude = lastValidLatitude;
    }

    public Double getLastValidLongitude() {
        return lastValidLongitude;
    }

    public void setLastValidLongitude(Double lastValidLongitude) {
        this.lastValidLongitude = lastValidLongitude;
    }

    public String getLatestPositions() {
        return latestPositions;
    }

    public void setLatestPositions(String latestPositions) {
        this.latestPositions = latestPositions;
    }

    public String getIconType() {
        return iconType;
    }

    public void setIconType(String iconType) {
        this.iconType = iconType;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Object getUserTimezoneId() {
        return userTimezoneId;
    }

    public void setUserTimezoneId(Object userTimezoneId) {
        this.userTimezoneId = userTimezoneId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

}
