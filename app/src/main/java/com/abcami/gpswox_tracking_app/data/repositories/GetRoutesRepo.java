package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.GetRouteState;
import com.abcami.gpswox_tracking_app.data.model.GetRoutes;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetRoutesRepo {

    private ApiClient apiClient;

    public GetRoutesRepo(Application application) {
        apiClient = new ApiClient(application);
    }

    public LiveData<GetRouteState> getRoutes(String key) {
        final MutableLiveData<GetRouteState> getRouteStateMutableLiveData = new MutableLiveData<>();
        Call<GetRoutes> call = apiClient.amiService().getRoutes(key);
        call.enqueue(new Callback<GetRoutes>() {
            @Override
            public void onResponse(Call<GetRoutes> call, Response<GetRoutes> response) {
                if (response.code() == 200) {
                    getRouteStateMutableLiveData.setValue(new GetRouteState(response.body()));
                } else {
                    getRouteStateMutableLiveData.setValue(new GetRouteState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GetRoutes> call, Throwable t) {
                getRouteStateMutableLiveData.setValue(new GetRouteState(t));
            }
        });
        return getRouteStateMutableLiveData;
    }
}
