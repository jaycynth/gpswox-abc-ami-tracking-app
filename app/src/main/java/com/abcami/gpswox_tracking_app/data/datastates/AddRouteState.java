package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.AddRoute;

public class AddRouteState {
    private AddRoute addRoute;
    private String message;
    private Throwable throwable;

    public AddRouteState(AddRoute addRoute) {
        this.addRoute = addRoute;
        this.message = null;
        this.throwable = null;
    }

    public AddRouteState(String message) {
        this.message = message;
        this.addRoute = null;
        this.throwable = null;
    }

    public AddRouteState(Throwable throwable) {
        this.throwable = throwable;
        this.addRoute = null;
        this.message = null;
    }

    public AddRoute getAddRoute() {
        return addRoute;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
