package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.UserLogin;

public class UserLoginState {

    private UserLogin userLogin;
    private String message;
    private Throwable throwable;

    public UserLoginState(UserLogin userLogin) {
        this.userLogin = userLogin;
        this.message = null;
        this.throwable = null;
    }

    public UserLoginState(String message) {
        this.message = message;
        this.userLogin = null;
        this.throwable = null;
    }

    public UserLoginState(Throwable throwable) {
        this.throwable = throwable;
        this.message = null;
        this.userLogin = null;
    }

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
