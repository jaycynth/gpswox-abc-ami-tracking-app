package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.AddGeofence;
import com.abcami.gpswox_tracking_app.data.model.AddGeofenceErrors;

public class AddGeofenceState {
    private AddGeofence addGeofence;

    //400 bad request
    private AddGeofenceErrors addGeofenceErrors;
    private String message;
    private Throwable throwable;


    public AddGeofenceState(AddGeofence addGeofence) {
        this.addGeofence = addGeofence;
        this.addGeofenceErrors = null;
        this.message = null;
        this.throwable = null;
    }

    public AddGeofenceState(AddGeofenceErrors addGeofenceErrors) {
        this.addGeofenceErrors = addGeofenceErrors;
        this.addGeofence = null;
        this.message = null;
        this.throwable = null;
    }

    public AddGeofenceState(String message) {
        this.message = message;
        this.addGeofence = null;
        this.addGeofenceErrors = null;
        this.throwable = null;

    }

    public AddGeofenceState(Throwable throwable) {
        this.throwable = throwable;
        this.addGeofence = null;
        this.addGeofenceErrors = null;
        this.message = null;
    }

    public AddGeofence getAddGeofence() {
        return addGeofence;
    }

    public void setAddGeofence(AddGeofence addGeofence) {
        this.addGeofence = addGeofence;
    }

    public AddGeofenceErrors getAddGeofenceErrors() {
        return addGeofenceErrors;
    }

    public void setAddGeofenceErrors(AddGeofenceErrors addGeofenceErrors) {
        this.addGeofenceErrors = addGeofenceErrors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}
