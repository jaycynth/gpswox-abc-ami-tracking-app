package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.DestroyRouteState;
import com.abcami.gpswox_tracking_app.data.model.DestroyRoute;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DestroyRouteRepo {
    private ApiClient apiClient;

    public DestroyRouteRepo(Application application) {
        apiClient = new ApiClient(application);
    }

    public LiveData<DestroyRouteState> destroyRoute(String key, int routeId) {
        final MutableLiveData<DestroyRouteState> destroyRouteStateMutableLiveData = new MutableLiveData<>();
        Call<DestroyRoute> call = apiClient.amiService().destroyRoute(key, routeId);
        call.enqueue(new Callback<DestroyRoute>() {
            @Override
            public void onResponse(Call<DestroyRoute> call, Response<DestroyRoute> response) {
                if (response.code() == 200) {
                    destroyRouteStateMutableLiveData.setValue(new DestroyRouteState(response.body()));
                } else {
                    destroyRouteStateMutableLiveData.setValue(new DestroyRouteState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<DestroyRoute> call, Throwable t) {
                destroyRouteStateMutableLiveData.setValue(new DestroyRouteState(t));

            }
        });
         return destroyRouteStateMutableLiveData;
    }
}
