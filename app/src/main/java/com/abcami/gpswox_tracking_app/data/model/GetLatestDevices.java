package com.abcami.gpswox_tracking_app.data.model;

import com.abcami.gpswox_tracking_app.data.model.latestDevices.Item;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetLatestDevices {
    @SerializedName("items")
    @Expose
    private List<com.abcami.gpswox_tracking_app.data.model.latestDevices.Item> items = null;
    @SerializedName("events")
    @Expose
    private List<Object> events = null;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("version")
    @Expose
    private String version;

    public List<com.abcami.gpswox_tracking_app.data.model.latestDevices.Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Object> getEvents() {
        return events;
    }

    public void setEvents(List<Object> events) {
        this.events = events;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
