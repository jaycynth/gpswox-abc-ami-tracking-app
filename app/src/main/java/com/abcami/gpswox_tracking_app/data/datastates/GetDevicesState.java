package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.GetDevices;

import java.util.List;

public class GetDevicesState {

    private List<GetDevices> allDevices;
    private String message;
    private Throwable throwable;

    public GetDevicesState(List<GetDevices> allDevices) {
        this.allDevices = allDevices;
        this.message = null;
        this.throwable = null;
    }

    public GetDevicesState(String message) {
        this.message = message;
        this.allDevices = null;
        this.throwable = null;
    }

    public GetDevicesState(Throwable throwable) {
        this.throwable = throwable;
        this.allDevices = null;
        this.message = null;
    }

    public List<GetDevices> getAllDevices() {
        return allDevices;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
