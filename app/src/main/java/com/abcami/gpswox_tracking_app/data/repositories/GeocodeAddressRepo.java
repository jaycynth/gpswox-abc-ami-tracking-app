package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.GeocodeAddressState;
import com.abcami.gpswox_tracking_app.data.model.GeocodeAddress;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeocodeAddressRepo {
    private ApiClient apiClient;

    public GeocodeAddressRepo(Application application) {
        apiClient = new ApiClient(application);
    }

    public LiveData<GeocodeAddressState> geocodeAddress(double lat, double lng){
        final MutableLiveData<GeocodeAddressState> geocodeAddressStateMutableLiveData = new MutableLiveData<>();

        Call<GeocodeAddress> call = apiClient.abcService().geocodeAddress(lat, lng);

        call.enqueue(new Callback<GeocodeAddress>() {
            @Override
            public void onResponse(Call<GeocodeAddress> call, Response<GeocodeAddress> response) {
                if(response.code() == 200){
                    geocodeAddressStateMutableLiveData.setValue(new GeocodeAddressState(response.body()));
                }else{
                    geocodeAddressStateMutableLiveData.setValue(new GeocodeAddressState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GeocodeAddress> call, Throwable t) {
              geocodeAddressStateMutableLiveData.setValue(new GeocodeAddressState(t));
            }
        });

        return geocodeAddressStateMutableLiveData;
    }
}
