package com.abcami.gpswox_tracking_app.data.model;

import com.google.android.gms.tasks.Tasks;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Permissions {

    @SerializedName("devices")
    @Expose
    private Devices devices;
    @SerializedName("alerts")
    @Expose
    private Alerts alerts;
    @SerializedName("geofences")
    @Expose
    private Geofences geofences;
    @SerializedName("routes")
    @Expose
    private Routes routes;
    @SerializedName("poi")
    @Expose
    private Poi poi;
    @SerializedName("reports")
    @Expose
    private Reports reports;
    @SerializedName("sms_gateway")
    @Expose
    private SmsGateway smsGateway;
    @SerializedName("send_command")
    @Expose
    private SendCommand sendCommand;
    @SerializedName("history")
    @Expose
    private History history;
    @SerializedName("maintenance")
    @Expose
    private Maintenance maintenance;
    @SerializedName("camera")
    @Expose
    private Camera camera;
    @SerializedName("device_camera")
    @Expose
    private DeviceCamera deviceCamera;
    @SerializedName("tasks")
    @Expose
    private Tasks tasks;
    @SerializedName("chat")
    @Expose
    private Chat chat;
    @SerializedName("device.imei")
    @Expose
    private DeviceImei deviceImei;
    @SerializedName("device.sim_number")
    @Expose
    private DeviceSimNumber deviceSimNumber;
    @SerializedName("device.forward")
    @Expose
    private DeviceForward deviceForward;
    @SerializedName("device.protocol")
    @Expose
    private DeviceProtocol deviceProtocol;
    @SerializedName("device.expiration_date")
    @Expose
    private DeviceExpirationDate deviceExpirationDate;
    @SerializedName("device.installation_date")
    @Expose
    private DeviceInstallationDate deviceInstallationDate;
    @SerializedName("device.sim_activation_date")
    @Expose
    private DeviceSimActivationDate deviceSimActivationDate;
    @SerializedName("device.sim_expiration_date")
    @Expose
    private DeviceSimExpirationDate deviceSimExpirationDate;
    @SerializedName("device.msisdn")
    @Expose
    private DeviceMsisdn deviceMsisdn;
    @SerializedName("sharing")
    @Expose
    private Sharing sharing;
    @SerializedName("checklist_template")
    @Expose
    private ChecklistTemplate checklistTemplate;
    @SerializedName("checklist")
    @Expose
    private Checklist checklist;
    @SerializedName("checklist_activity")
    @Expose
    private ChecklistActivity checklistActivity;
    @SerializedName("checklist_qr_code")
    @Expose
    private ChecklistQrCode checklistQrCode;
    @SerializedName("checklist_qr_pre_start_only")
    @Expose
    private ChecklistQrPreStartOnly checklistQrPreStartOnly;
    @SerializedName("device_configuration")
    @Expose
    private DeviceConfiguration deviceConfiguration;
    @SerializedName("call_actions")
    @Expose
    private CallActions callActions;

    public Devices getDevices() {
        return devices;
    }

    public void setDevices(Devices devices) {
        this.devices = devices;
    }

    public Alerts getAlerts() {
        return alerts;
    }

    public void setAlerts(Alerts alerts) {
        this.alerts = alerts;
    }

    public Geofences getGeofences() {
        return geofences;
    }

    public void setGeofences(Geofences geofences) {
        this.geofences = geofences;
    }

    public Routes getRoutes() {
        return routes;
    }

    public void setRoutes(Routes routes) {
        this.routes = routes;
    }

    public Poi getPoi() {
        return poi;
    }

    public void setPoi(Poi poi) {
        this.poi = poi;
    }

    public Reports getReports() {
        return reports;
    }

    public void setReports(Reports reports) {
        this.reports = reports;
    }

    public SmsGateway getSmsGateway() {
        return smsGateway;
    }

    public void setSmsGateway(SmsGateway smsGateway) {
        this.smsGateway = smsGateway;
    }

    public SendCommand getSendCommand() {
        return sendCommand;
    }

    public void setSendCommand(SendCommand sendCommand) {
        this.sendCommand = sendCommand;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public Maintenance getMaintenance() {
        return maintenance;
    }

    public void setMaintenance(Maintenance maintenance) {
        this.maintenance = maintenance;
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public DeviceCamera getDeviceCamera() {
        return deviceCamera;
    }

    public void setDeviceCamera(DeviceCamera deviceCamera) {
        this.deviceCamera = deviceCamera;
    }

    public Tasks getTasks() {
        return tasks;
    }

    public void setTasks(Tasks tasks) {
        this.tasks = tasks;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public DeviceImei getDeviceImei() {
        return deviceImei;
    }

    public void setDeviceImei(DeviceImei deviceImei) {
        this.deviceImei = deviceImei;
    }

    public DeviceSimNumber getDeviceSimNumber() {
        return deviceSimNumber;
    }

    public void setDeviceSimNumber(DeviceSimNumber deviceSimNumber) {
        this.deviceSimNumber = deviceSimNumber;
    }

    public DeviceForward getDeviceForward() {
        return deviceForward;
    }

    public void setDeviceForward(DeviceForward deviceForward) {
        this.deviceForward = deviceForward;
    }

    public DeviceProtocol getDeviceProtocol() {
        return deviceProtocol;
    }

    public void setDeviceProtocol(DeviceProtocol deviceProtocol) {
        this.deviceProtocol = deviceProtocol;
    }

    public DeviceExpirationDate getDeviceExpirationDate() {
        return deviceExpirationDate;
    }

    public void setDeviceExpirationDate(DeviceExpirationDate deviceExpirationDate) {
        this.deviceExpirationDate = deviceExpirationDate;
    }

    public DeviceInstallationDate getDeviceInstallationDate() {
        return deviceInstallationDate;
    }

    public void setDeviceInstallationDate(DeviceInstallationDate deviceInstallationDate) {
        this.deviceInstallationDate = deviceInstallationDate;
    }

    public DeviceSimActivationDate getDeviceSimActivationDate() {
        return deviceSimActivationDate;
    }

    public void setDeviceSimActivationDate(DeviceSimActivationDate deviceSimActivationDate) {
        this.deviceSimActivationDate = deviceSimActivationDate;
    }

    public DeviceSimExpirationDate getDeviceSimExpirationDate() {
        return deviceSimExpirationDate;
    }

    public void setDeviceSimExpirationDate(DeviceSimExpirationDate deviceSimExpirationDate) {
        this.deviceSimExpirationDate = deviceSimExpirationDate;
    }

    public DeviceMsisdn getDeviceMsisdn() {
        return deviceMsisdn;
    }

    public void setDeviceMsisdn(DeviceMsisdn deviceMsisdn) {
        this.deviceMsisdn = deviceMsisdn;
    }

    public Sharing getSharing() {
        return sharing;
    }

    public void setSharing(Sharing sharing) {
        this.sharing = sharing;
    }

    public ChecklistTemplate getChecklistTemplate() {
        return checklistTemplate;
    }

    public void setChecklistTemplate(ChecklistTemplate checklistTemplate) {
        this.checklistTemplate = checklistTemplate;
    }

    public Checklist getChecklist() {
        return checklist;
    }

    public void setChecklist(Checklist checklist) {
        this.checklist = checklist;
    }

    public ChecklistActivity getChecklistActivity() {
        return checklistActivity;
    }

    public void setChecklistActivity(ChecklistActivity checklistActivity) {
        this.checklistActivity = checklistActivity;
    }

    public ChecklistQrCode getChecklistQrCode() {
        return checklistQrCode;
    }

    public void setChecklistQrCode(ChecklistQrCode checklistQrCode) {
        this.checklistQrCode = checklistQrCode;
    }

    public ChecklistQrPreStartOnly getChecklistQrPreStartOnly() {
        return checklistQrPreStartOnly;
    }

    public void setChecklistQrPreStartOnly(ChecklistQrPreStartOnly checklistQrPreStartOnly) {
        this.checklistQrPreStartOnly = checklistQrPreStartOnly;
    }

    public DeviceConfiguration getDeviceConfiguration() {
        return deviceConfiguration;
    }

    public void setDeviceConfiguration(DeviceConfiguration deviceConfiguration) {
        this.deviceConfiguration = deviceConfiguration;
    }

    public CallActions getCallActions() {
        return callActions;
    }

    public void setCallActions(CallActions callActions) {
        this.callActions = callActions;
    }

}




