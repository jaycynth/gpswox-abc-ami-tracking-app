package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.GeocodeAddress;

public class GeocodeAddressState {

    public GeocodeAddress geocodeAddress;
    private String message;
    private Throwable throwable;

    public GeocodeAddressState(GeocodeAddress geocodeAddress) {
        this.geocodeAddress = geocodeAddress;
        this.message =  null;
        this.throwable = null;
    }

    public GeocodeAddressState(String message) {
        this.message = message;
        this.geocodeAddress = null;
        this.throwable = null;
    }

    public GeocodeAddressState(Throwable throwable) {
        this.throwable = throwable;
        this.geocodeAddress = null;
        this.message = null;
    }

    public GeocodeAddress getGeocodeAddress() {
        return geocodeAddress;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
