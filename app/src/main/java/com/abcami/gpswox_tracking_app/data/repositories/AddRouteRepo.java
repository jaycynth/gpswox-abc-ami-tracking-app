package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.AddRouteState;
import com.abcami.gpswox_tracking_app.data.model.AddRoute;
import com.abcami.gpswox_tracking_app.data.model.Coordinate;
import com.abcami.gpswox_tracking_app.data.model.Polyline;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddRouteRepo {

    private ApiClient apiClient;

    public AddRouteRepo(Application application) {
        apiClient = new ApiClient(application);
    }

    public LiveData<AddRouteState> addRoute(String user_api_hash, String name, String color, String polylines) {
        final MutableLiveData<AddRouteState> addRouteStateMutableLiveData = new MutableLiveData<>();
        Call<AddRoute> call = apiClient.amiService().addRoute(user_api_hash, name, color, polylines);

        call.enqueue(new Callback<AddRoute>() {
            @Override
            public void onResponse(Call<AddRoute> call, Response<AddRoute> response) {
                if (response.code() == 200) {
                    addRouteStateMutableLiveData.setValue(new AddRouteState(response.body()));
                } else {
                    addRouteStateMutableLiveData.setValue(new AddRouteState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AddRoute> call, Throwable t) {
                addRouteStateMutableLiveData.setValue(new AddRouteState(t));

            }
        });
        return addRouteStateMutableLiveData;
    }
}
