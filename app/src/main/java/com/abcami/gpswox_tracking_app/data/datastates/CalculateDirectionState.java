package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.CalculateDirections;

public class CalculateDirectionState {
    private CalculateDirections calculateDirections;
    private String message;
    private Throwable throwable;

    public CalculateDirectionState(CalculateDirections calculateDirections) {
        this.calculateDirections = calculateDirections;
        this.message = null;
        this.throwable = null;
    }

    public CalculateDirectionState(String message) {
        this.message = message;
        this.calculateDirections = null;
        this.throwable = null;
    }

    public CalculateDirectionState(Throwable throwable) {
        this.throwable = throwable;
        this.calculateDirections = null;
        this.message = null;
    }

    public CalculateDirections getCalculateDirections() {
        return calculateDirections;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
