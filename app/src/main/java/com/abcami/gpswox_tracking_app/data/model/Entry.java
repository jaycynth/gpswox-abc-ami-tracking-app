package com.abcami.gpswox_tracking_app.data.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.UUID;

public class Entry {

    private String id = UUID.randomUUID().toString();
    private String TYPE = "circle";
    private String POLYGON_COLOR = "#03DAC5";

    private LatLng latLng;
    private Double radius;

    //name
    private String message;

    public Entry() {
    }

    public Entry(LatLng latLng, Double radius, String message) {
        this.latLng = latLng;
        this.radius = radius;
        this.message = message;
    }


    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getPOLYGON_COLOR() {
        return POLYGON_COLOR;
    }

    public void setPOLYGON_COLOR(String POLYGON_COLOR) {
        this.POLYGON_COLOR = POLYGON_COLOR;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
