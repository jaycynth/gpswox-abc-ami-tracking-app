package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.DestroyGeofenceState;
import com.abcami.gpswox_tracking_app.data.model.DestroyGeofence;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DestroyGeofenceRepo {

    private ApiClient apiClient;

    public DestroyGeofenceRepo(Application application) {
        apiClient = new ApiClient(application);
    }

    public LiveData<DestroyGeofenceState> destroyGeofence(String user_api_hash, int geofence_id) {
        final MutableLiveData<DestroyGeofenceState> destroyGeofenceStateMutableLiveData = new MutableLiveData<>();

        Call<DestroyGeofence> call = apiClient.amiService().destroyGeofence(user_api_hash, geofence_id);
        call.enqueue(new Callback<DestroyGeofence>() {
            @Override
            public void onResponse(Call<DestroyGeofence> call, Response<DestroyGeofence> response) {
                if (response.code() == 200) {
                    destroyGeofenceStateMutableLiveData.setValue(new DestroyGeofenceState(response.body()));
                } else {
                    destroyGeofenceStateMutableLiveData.setValue(new DestroyGeofenceState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<DestroyGeofence> call, Throwable t) {
                destroyGeofenceStateMutableLiveData.setValue(new DestroyGeofenceState(t));
            }
        });

        return destroyGeofenceStateMutableLiveData;
    }
}
