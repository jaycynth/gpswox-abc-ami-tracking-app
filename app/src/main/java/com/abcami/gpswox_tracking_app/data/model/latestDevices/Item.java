package com.abcami.gpswox_tracking_app.data.model.latestDevices;

import com.abcami.gpswox_tracking_app.data.model.DeviceData;
import com.abcami.gpswox_tracking_app.data.model.DriverData;
import com.abcami.gpswox_tracking_app.data.model.Icon;
import com.abcami.gpswox_tracking_app.data.model.IconColors;
import com.abcami.gpswox_tracking_app.data.model.Sensors;
import com.abcami.gpswox_tracking_app.data.model.Tail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Item {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("alarm")
    @Expose
    private Integer alarm;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("online")
    @Expose
    private String online;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;
    @SerializedName("acktimestamp")
    @Expose
    private Integer acktimestamp;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("course")
    @Expose
    private Integer course;
    @SerializedName("speed")
    @Expose
    private Integer speed;
    @SerializedName("altitude")
    @Expose
    private Integer altitude;
    @SerializedName("icon_type")
    @Expose
    private String iconType;
    @SerializedName("icon_color")
    @Expose
    private String iconColor;
    @SerializedName("icon_colors")
    @Expose
    private IconColors iconColors;
    @SerializedName("icon")
    @Expose
    private Icon icon;
    @SerializedName("power")
    @Expose
    private String power;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("protocol")
    @Expose
    private String protocol;
    @SerializedName("driver")
    @Expose
    private String driver;
    @SerializedName("driver_data")
    @Expose
    private DriverData driverData;
    @SerializedName("sensors")
    @Expose
    private String sensors = null;
    @SerializedName("services")
    @Expose
    private String services = null;
    @SerializedName("tail")
    @Expose
    private String tail = null;
    @SerializedName("distance_unit_hour")
    @Expose
    private String distanceUnitHour;
    @SerializedName("unit_of_distance")
    @Expose
    private String unitOfDistance;
    @SerializedName("unit_of_altitude")
    @Expose
    private String unitOfAltitude;
    @SerializedName("unit_of_capacity")
    @Expose
    private String unitOfCapacity;
    @SerializedName("stop_duration")
    @Expose
    private String stopDuration;
    @SerializedName("moved_timestamp")
    @Expose
    private Integer movedTimestamp;
    @SerializedName("engine_status")
    @Expose
    private Object engineStatus;
    @SerializedName("detect_engine")
    @Expose
    private String detectEngine;
    @SerializedName("engine_hours")
    @Expose
    private String engineHours;
    @SerializedName("total_distance")
    @Expose
    private double totalDistance;
    @SerializedName("device_data")
    @Expose
    private DeviceData deviceData;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAlarm() {
        return alarm;
    }

    public void setAlarm(Integer alarm) {
        this.alarm = alarm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getAcktimestamp() {
        return acktimestamp;
    }

    public void setAcktimestamp(Integer acktimestamp) {
        this.acktimestamp = acktimestamp;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getAltitude() {
        return altitude;
    }

    public void setAltitude(Integer altitude) {
        this.altitude = altitude;
    }

    public String getIconType() {
        return iconType;
    }

    public void setIconType(String iconType) {
        this.iconType = iconType;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public IconColors getIconColors() {
        return iconColors;
    }

    public void setIconColors(IconColors iconColors) {
        this.iconColors = iconColors;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public DriverData getDriverData() {
        return driverData;
    }

    public void setDriverData(DriverData driverData) {
        this.driverData = driverData;
    }

    public String getSensors() {
        return sensors;
    }

    public void setSensors(String sensors) {
        this.sensors = sensors;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getTail() {
        return tail;
    }

    public void setTail(String tail) {
        this.tail = tail;
    }

    public String getDistanceUnitHour() {
        return distanceUnitHour;
    }

    public void setDistanceUnitHour(String distanceUnitHour) {
        this.distanceUnitHour = distanceUnitHour;
    }

    public String getUnitOfDistance() {
        return unitOfDistance;
    }

    public void setUnitOfDistance(String unitOfDistance) {
        this.unitOfDistance = unitOfDistance;
    }

    public String getUnitOfAltitude() {
        return unitOfAltitude;
    }

    public void setUnitOfAltitude(String unitOfAltitude) {
        this.unitOfAltitude = unitOfAltitude;
    }

    public String getUnitOfCapacity() {
        return unitOfCapacity;
    }

    public void setUnitOfCapacity(String unitOfCapacity) {
        this.unitOfCapacity = unitOfCapacity;
    }

    public String getStopDuration() {
        return stopDuration;
    }

    public void setStopDuration(String stopDuration) {
        this.stopDuration = stopDuration;
    }

    public Integer getMovedTimestamp() {
        return movedTimestamp;
    }

    public void setMovedTimestamp(Integer movedTimestamp) {
        this.movedTimestamp = movedTimestamp;
    }

    public Object getEngineStatus() {
        return engineStatus;
    }

    public void setEngineStatus(Object engineStatus) {
        this.engineStatus = engineStatus;
    }

    public String getDetectEngine() {
        return detectEngine;
    }

    public void setDetectEngine(String detectEngine) {
        this.detectEngine = detectEngine;
    }

    public String getEngineHours() {
        return engineHours;
    }

    public void setEngineHours(String engineHours) {
        this.engineHours = engineHours;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public DeviceData getDeviceData() {
        return deviceData;
    }

    public void setDeviceData(DeviceData deviceData) {
        this.deviceData = deviceData;
    }

}
