package com.abcami.gpswox_tracking_app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.security.Permissions;

public class UserLogin {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("user_api_hash")
    @Expose
    private String userApiHash;
    @SerializedName("permissions")
    @Expose
    private Permissions permissions;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserApiHash() {
        return userApiHash;
    }

    public void setUserApiHash(String userApiHash) {
        this.userApiHash = userApiHash;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    public void setPermissions(Permissions permissions) {
        this.permissions = permissions;
    }

}
