package com.abcami.gpswox_tracking_app.data.repositories;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.core.content.ContextCompat;

import com.abcami.gpswox_tracking_app.data.model.Entry;
import com.abcami.gpswox_tracking_app.utils.GeofenceBroadcastReceiver;
import com.abcami.gpswox_tracking_app.utils.GeofenceErrorMessages;
import com.abcami.gpswox_tracking_app.utils.RepoResult;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.abcami.gpswox_tracking_app.utils.Constants.PREFS_NAME;
import static com.abcami.gpswox_tracking_app.utils.Constants.REMINDERS;
import static com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_ENTER;


public class GeofenceEntryRepo {

    private Context context;
    private GeofencingClient geofencingClient;
    private PendingIntent geofencePendingIntent;

    private SharedPreferences preferences;
    private Gson gson = new Gson();

    public GeofenceEntryRepo(Context context) {
        this.context = context;
        geofencingClient = LocationServices.getGeofencingClient(Objects.requireNonNull(context));
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

    }

    public void add(Entry entry, RepoResult repoResult) {

        Geofence geofence = buildGeofence(entry);

        if (geofence != null
                && ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            geofencingClient.addGeofences(buildGeofencingRequest(geofence), geofencePendingIntent())
                    .addOnSuccessListener(aVoid -> {
                        //save the entry
                        List<Entry> list = getAll();
                        list.add(entry);
                        saveAll(list);
                        repoResult.result(null, "SUCCESS..!! ENTRY ADDED");
                    })
                    .addOnFailureListener(e -> repoResult.result(GeofenceErrorMessages.getErrorString(context, e), null));

        }
    }

    //creates a geofence instance based on the entry given
    private Geofence buildGeofence(Entry entry) {

        Double latitude = entry.getLatLng().latitude;
        Double longitude = entry.getLatLng().longitude;
        double radius = entry.getRadius();

        if (latitude != null && longitude != null && String.valueOf(radius) != null) {
            return new Geofence.Builder()
                    .setRequestId(entry.getId())
                    .setCircularRegion(
                            latitude, longitude,
                            (float) radius
                    )
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .build();
        }

        return null;
    }

    //The GeofencingRequest class receives the geofences that should be monitored
    //takes a geofence instance and the initial trigger
    private GeofencingRequest buildGeofencingRequest(Geofence geofence) {
        List<Geofence> geofences = new ArrayList<>();
        geofences.add(geofence);
        return new GeofencingRequest.Builder()
                .setInitialTrigger(INITIAL_TRIGGER_ENTER)
                .addGeofences(geofences)
                .build();
    }

    //to call IntentService that will handle a GeofenceEvent
    private PendingIntent geofencePendingIntent() {

        if (geofencePendingIntent != null) {
            return geofencePendingIntent;
        }
        Intent intent = new Intent(context, GeofenceBroadcastReceiver.class);
        geofencePendingIntent = PendingIntent.getBroadcast(
                context,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        return geofencePendingIntent;
    }


    private void saveAll(List<Entry> list) {
        preferences
                .edit()
                .putString(REMINDERS, gson.toJson(list))
                .apply();
    }

    public List<Entry> getAll() {
        List<Entry> arrayOfReminders = new ArrayList<>();
        if (preferences.contains(REMINDERS)) {
            String remindersString = preferences.getString(REMINDERS, null);


            Gson gson = new Gson();
            Type myList = new TypeToken<ArrayList<Entry>>() {
            }.getType();
            arrayOfReminders = gson.fromJson(remindersString, myList);

        }
        return arrayOfReminders;
    }

    public Entry get(String requestId) {
        for (Entry i : getAll()) {
            if (i != null) {
                if (i.getId().equals(requestId)) {
                    return i;
                }
            }
        }
        return null;

    }

    public void remove(Entry entry, RepoResult repoResult) {
        geofencingClient
                .removeGeofences(geofencePendingIntent)
                .addOnSuccessListener(
                        aVoid -> {
                            List<Entry> list1 = getAll();
                            list1.remove(list1.size() - 1);

                            Log.d("LIST", list1.toString());
                            saveAll(list1);
                            repoResult.result(null, "SUCCESS REMOVED");
                        }
                )
                .addOnFailureListener(e -> repoResult.result(GeofenceErrorMessages.getErrorString(context, e), null));
    }
}




