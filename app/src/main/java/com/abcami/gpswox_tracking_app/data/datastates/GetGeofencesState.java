package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.GetGeofences;

public class GetGeofencesState {

    private GetGeofences geofences;
    private String message;
    private Throwable throwable;

    public GetGeofencesState(GetGeofences geofences) {
        this.geofences = geofences;
        this.message = null;
        this.throwable = null;
    }

    public GetGeofencesState(String message) {
        this.message = message;
        this.geofences = null;
        this.throwable = null;
    }

    public GetGeofencesState(Throwable throwable) {
        this.throwable = throwable;
        this.geofences = null;
        this.message = null;
    }

    public GetGeofences getGeofences() {
        return geofences;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
