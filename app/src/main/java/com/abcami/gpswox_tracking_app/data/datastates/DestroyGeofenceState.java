package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.DestroyGeofence;

public class DestroyGeofenceState {

    private DestroyGeofence destroyGeofence;
    private String message;
    private Throwable throwable;

    public DestroyGeofenceState(DestroyGeofence destroyGeofence) {
        this.destroyGeofence = destroyGeofence;
        this.message = null;
        this.throwable = null;
    }

    public DestroyGeofenceState(String message) {
        this.message = message;
        this.destroyGeofence = null;
        this.throwable = null;
    }

    public DestroyGeofenceState(Throwable throwable) {
        this.throwable = throwable;
        this.destroyGeofence = null;
        this.message = null;
    }

    public DestroyGeofence getDestroyGeofence() {
        return destroyGeofence;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
