package com.abcami.gpswox_tracking_app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sensors {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("tag_name")
    @Expose
    private String tagName;
    @SerializedName("add_to_history")
    @Expose
    private String addToHistory;
    @SerializedName("on_value")
    @Expose
    private String onValue;
    @SerializedName("off_value")
    @Expose
    private String offValue;
    @SerializedName("shown_value_by")
    @Expose
    private Object shownValueBy;
    @SerializedName("fuel_tank_name")
    @Expose
    private Object fuelTankName;
    @SerializedName("full_tank")
    @Expose
    private Object fullTank;
    @SerializedName("full_tank_value")
    @Expose
    private Object fullTankValue;
    @SerializedName("min_value")
    @Expose
    private Object minValue;
    @SerializedName("max_value")
    @Expose
    private Object maxValue;
    @SerializedName("formula")
    @Expose
    private Object formula;
    @SerializedName("odometer_value_by")
    @Expose
    private Object odometerValueBy;
    @SerializedName("odometer_value")
    @Expose
    private Object odometerValue;
    @SerializedName("odometer_value_unit")
    @Expose
    private String odometerValueUnit;
    @SerializedName("temperature_max")
    @Expose
    private Object temperatureMax;
    @SerializedName("temperature_max_value")
    @Expose
    private Object temperatureMaxValue;
    @SerializedName("temperature_min")
    @Expose
    private Object temperatureMin;
    @SerializedName("temperature_min_value")
    @Expose
    private Object temperatureMinValue;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("value_formula")
    @Expose
    private String valueFormula;
    @SerializedName("show_in_popup")
    @Expose
    private String showInPopup;
    @SerializedName("unit_of_measurement")
    @Expose
    private String unitOfMeasurement;
    @SerializedName("on_tag_value")
    @Expose
    private Object onTagValue;
    @SerializedName("off_tag_value")
    @Expose
    private Object offTagValue;
    @SerializedName("on_type")
    @Expose
    private Object onType;
    @SerializedName("off_type")
    @Expose
    private Object offType;
    @SerializedName("calibrations")
    @Expose
    private Object calibrations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getAddToHistory() {
        return addToHistory;
    }

    public void setAddToHistory(String addToHistory) {
        this.addToHistory = addToHistory;
    }

    public String getOnValue() {
        return onValue;
    }

    public void setOnValue(String onValue) {
        this.onValue = onValue;
    }

    public String getOffValue() {
        return offValue;
    }

    public void setOffValue(String offValue) {
        this.offValue = offValue;
    }

    public Object getShownValueBy() {
        return shownValueBy;
    }

    public void setShownValueBy(Object shownValueBy) {
        this.shownValueBy = shownValueBy;
    }

    public Object getFuelTankName() {
        return fuelTankName;
    }

    public void setFuelTankName(Object fuelTankName) {
        this.fuelTankName = fuelTankName;
    }

    public Object getFullTank() {
        return fullTank;
    }

    public void setFullTank(Object fullTank) {
        this.fullTank = fullTank;
    }

    public Object getFullTankValue() {
        return fullTankValue;
    }

    public void setFullTankValue(Object fullTankValue) {
        this.fullTankValue = fullTankValue;
    }

    public Object getMinValue() {
        return minValue;
    }

    public void setMinValue(Object minValue) {
        this.minValue = minValue;
    }

    public Object getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Object maxValue) {
        this.maxValue = maxValue;
    }

    public Object getFormula() {
        return formula;
    }

    public void setFormula(Object formula) {
        this.formula = formula;
    }

    public Object getOdometerValueBy() {
        return odometerValueBy;
    }

    public void setOdometerValueBy(Object odometerValueBy) {
        this.odometerValueBy = odometerValueBy;
    }

    public Object getOdometerValue() {
        return odometerValue;
    }

    public void setOdometerValue(Object odometerValue) {
        this.odometerValue = odometerValue;
    }

    public String getOdometerValueUnit() {
        return odometerValueUnit;
    }

    public void setOdometerValueUnit(String odometerValueUnit) {
        this.odometerValueUnit = odometerValueUnit;
    }

    public Object getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(Object temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public Object getTemperatureMaxValue() {
        return temperatureMaxValue;
    }

    public void setTemperatureMaxValue(Object temperatureMaxValue) {
        this.temperatureMaxValue = temperatureMaxValue;
    }

    public Object getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(Object temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public Object getTemperatureMinValue() {
        return temperatureMinValue;
    }

    public void setTemperatureMinValue(Object temperatureMinValue) {
        this.temperatureMinValue = temperatureMinValue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueFormula() {
        return valueFormula;
    }

    public void setValueFormula(String valueFormula) {
        this.valueFormula = valueFormula;
    }

    public String getShowInPopup() {
        return showInPopup;
    }

    public void setShowInPopup(String showInPopup) {
        this.showInPopup = showInPopup;
    }

    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setUnitOfMeasurement(String unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }

    public Object getOnTagValue() {
        return onTagValue;
    }

    public void setOnTagValue(Object onTagValue) {
        this.onTagValue = onTagValue;
    }

    public Object getOffTagValue() {
        return offTagValue;
    }

    public void setOffTagValue(Object offTagValue) {
        this.offTagValue = offTagValue;
    }

    public Object getOnType() {
        return onType;
    }

    public void setOnType(Object onType) {
        this.onType = onType;
    }

    public Object getOffType() {
        return offType;
    }

    public void setOffType(Object offType) {
        this.offType = offType;
    }

    public Object getCalibrations() {
        return calibrations;
    }

    public void setCalibrations(Object calibrations) {
        this.calibrations = calibrations;
    }

}
