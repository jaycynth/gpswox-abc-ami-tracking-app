package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.GetRoutes;

public class GetRouteState {
    private GetRoutes routeState;
    private String message;
    private Throwable throwable;

    public GetRouteState(GetRoutes routeState) {
        this.routeState = routeState;
        this.message = null;
        this.throwable = null;
    }

    public GetRouteState(String message) {
        this.message = message;
        this.routeState = null;
        this.throwable  = null;
    }

    public GetRouteState(Throwable throwable) {
        this.throwable = throwable;
        this.routeState = null;
        this.message = null;
    }

    public GetRoutes getRouteState() {
        return routeState;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
