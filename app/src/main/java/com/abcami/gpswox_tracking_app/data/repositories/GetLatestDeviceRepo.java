package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.GetLatestDevicesState;
import com.abcami.gpswox_tracking_app.data.model.GetLatestDevices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetLatestDeviceRepo {

    private ApiClient apiClient;

    public GetLatestDeviceRepo(Application application) {
        apiClient = new ApiClient(application);
    }

    public LiveData<GetLatestDevicesState> getLatestDevics(String key, int time) {
        final MutableLiveData<GetLatestDevicesState> getLatestDevicesStateMutableLiveData = new MutableLiveData<>();
        Call<GetLatestDevices> call = apiClient.amiService().getLatestDevices(key, time);

        call.enqueue(new Callback<GetLatestDevices>() {
            @Override
            public void onResponse(Call<GetLatestDevices> call, Response<GetLatestDevices> response) {
                if (response.code() == 200) {
                    getLatestDevicesStateMutableLiveData.setValue(new GetLatestDevicesState(response.body()));
                } else {
                    getLatestDevicesStateMutableLiveData.setValue(new GetLatestDevicesState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GetLatestDevices> call, Throwable t) {
                getLatestDevicesStateMutableLiveData.setValue(new GetLatestDevicesState(t));
            }
        });

        return getLatestDevicesStateMutableLiveData;
    }
}
