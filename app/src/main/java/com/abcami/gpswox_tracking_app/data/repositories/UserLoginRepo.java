package com.abcami.gpswox_tracking_app.data.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.abcami.gpswox_tracking_app.api.ApiClient;
import com.abcami.gpswox_tracking_app.data.datastates.UserLoginState;
import com.abcami.gpswox_tracking_app.data.model.UserLogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserLoginRepo {

 private ApiClient mApiClient;

    public UserLoginRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<UserLoginState> userLogin(String email, String password) {
        final MutableLiveData<UserLoginState> UserLoginStateMutableLiveData = new MutableLiveData<>();
        Call<UserLogin> call = mApiClient.amiService().userLogin(email, password);
        call.enqueue(new Callback<UserLogin>() {
            @Override
            public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
                if (response.code() == 200) {
                    UserLoginStateMutableLiveData.setValue(new UserLoginState(response.body()));

                }
                else {
                    UserLoginStateMutableLiveData.setValue(new UserLoginState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<UserLogin> call, Throwable t) {
                UserLoginStateMutableLiveData.setValue(new UserLoginState(t));
            }
        });

        return UserLoginStateMutableLiveData;


    }
}
