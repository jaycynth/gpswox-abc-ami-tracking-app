package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.GetLatestDevices;

public class GetLatestDevicesState {
    private GetLatestDevices latestDevices;
    private String message;
    private Throwable throwable;

    public GetLatestDevicesState(GetLatestDevices latestDevices) {
        this.latestDevices = latestDevices;
        this.message = null;
        this.throwable = null;
    }

    public GetLatestDevicesState(String message) {
        this.message = message;
        this.latestDevices = null;
        this.throwable = null;
    }

    public GetLatestDevicesState(Throwable throwable) {
        this.throwable = throwable;
        this.latestDevices = null;
        this.message = null;
    }

    public GetLatestDevices getLatestDevices() {
        return latestDevices;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
