package com.abcami.gpswox_tracking_app.data.datastates;

import com.abcami.gpswox_tracking_app.data.model.DestroyRoute;

public class DestroyRouteState {
    private DestroyRoute destroyRoute;
    private String message;
    private Throwable throwable;

    public DestroyRouteState(DestroyRoute destroyRoute) {
        this.destroyRoute = destroyRoute;
        this.message = null;
        this.throwable = null;
    }

    public DestroyRouteState(String message) {
        this.message = message;
        this.destroyRoute = null;
        this.throwable = null;
    }

    public DestroyRouteState(Throwable throwable) {
        this.throwable = throwable;
        this.destroyRoute = null;
        this.message = null;
    }

    public DestroyRoute getDestroyRoute() {
        return destroyRoute;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
